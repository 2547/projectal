package com.adaptaconsultoria.alc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.adaptaconsultoria.alc.models.EmailSender;
import com.adaptaconsultoria.alc.models.Parameter;
import com.adaptaconsultoria.alc.repositories.ViewRepository;
import com.adaptaconsultoria.alc.services.AuthService;
import com.adaptaconsultoria.alc.services.EmailSenderService;
import com.adaptaconsultoria.alc.services.EmailService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTests {
	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	private AuthService authService;

	@Autowired
	ViewRepository viewRepository;

	@Autowired
	EmailService emailService;
	
	@Autowired
	EmailSenderService emailSenderService;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		emailSender();
	}

	public void emailSender() {
		EmailSender emailSender = new EmailSender();
		emailSender.setId(23L);
		emailSender.setParameters(getParameters());
		emailSender.setReceivers(new ArrayList<>(Arrays.asList("orthonny@gmail.com")));
		emailSenderService.prepareToSend(emailSender);
	}
	
	public List<Parameter> getParameters() {
		List<Parameter> ps = new ArrayList<>();
		Parameter p = new Parameter();
		p.setName("username");
		p.setValue("Maria");
		ps.add(p);
		p = new Parameter();
		p.setName("login");
		p.setValue("maria");
		ps.add(p);
		return ps;
	}

	public void postRequest(String url, Object param) {
		try {
			MvcResult result = mockMvc.perform(post("/campaings?companyId=34").contentType(MediaType.APPLICATION_JSON)
					.content("json").characterEncoding("utf-8")).andExpect(status().isOk()).andReturn();
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}






















