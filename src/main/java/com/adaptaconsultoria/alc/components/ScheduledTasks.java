package com.adaptaconsultoria.alc.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.adaptaconsultoria.alc.services.EmailService;

@Component
public class ScheduledTasks {
	
    @Autowired
	private EmailService emailService;

    //	This method will look forward all pending emails and send them every 30 seconds
    @Scheduled(fixedDelay = 30000)
    public void emailSender() {
    	System.out.println(emailService.runGenerateQueueEmail());
    	emailService.loadEmailsSetAndSend();
    }
}
