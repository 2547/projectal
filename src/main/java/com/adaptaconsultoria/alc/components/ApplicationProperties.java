package com.adaptaconsultoria.alc.components;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties
{
	@Value("${application.token}")
	public String token;
	
	@Value("${application.password}")
	public String password;
}
