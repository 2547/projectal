package com.adaptaconsultoria.alc.objects.out;

import com.adaptaconsultoria.alc.objects.out.error.AuthPostError;

import lombok.Data;

@Data
public class AuthPostOut {
	private String token;

	private boolean hasError = false;
	private AuthPostError error;
}
