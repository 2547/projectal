package com.adaptaconsultoria.alc.objects.out;

import com.adaptaconsultoria.alc.objects.out.error.DefaultError;

import lombok.Data;

@Data
public class DefaultOut {
	private String token;
	private boolean hasError = false;
	private DefaultError error;
}
