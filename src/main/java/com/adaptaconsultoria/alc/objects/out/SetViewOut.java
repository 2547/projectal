package com.adaptaconsultoria.alc.objects.out;

import lombok.Data;

@Data
public class SetViewOut {
	private String email;
	private String param;
	private String nextPhaseUrl;
}
