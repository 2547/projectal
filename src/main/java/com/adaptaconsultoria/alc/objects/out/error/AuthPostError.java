package com.adaptaconsultoria.alc.objects.out.error;

import lombok.Data;

@Data
public class AuthPostError {

	private String error;

	private String appToken;
	private String appPassword;
	private String userName;
	private String userPassword;
	private String ipAddress;

}
