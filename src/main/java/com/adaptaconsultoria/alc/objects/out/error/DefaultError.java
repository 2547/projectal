package com.adaptaconsultoria.alc.objects.out.error;

import lombok.Data;

@Data
public class DefaultError {

	private String error;

}
