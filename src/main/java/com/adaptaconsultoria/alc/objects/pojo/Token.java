package com.adaptaconsultoria.alc.objects.pojo;

import lombok.Data;

@Data
public class Token {

	private String token;
	private String error;

}
