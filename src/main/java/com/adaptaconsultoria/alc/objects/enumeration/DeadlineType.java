package com.adaptaconsultoria.alc.objects.enumeration;

public enum DeadlineType {
	m("Minuto/s"), h("Hora/s"), d("Dia/s");
	
	private String description;

	DeadlineType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
