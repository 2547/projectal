package com.adaptaconsultoria.alc.objects.in;

import com.adaptaconsultoria.alc.models.EmailSender;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true)
public class EmailSenderIn {
	@JsonProperty("emailSender")
	private EmailSender emailSender;
}
