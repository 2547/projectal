package com.adaptaconsultoria.alc.objects.in;

import java.util.Date;

import lombok.Data;

@Data
public class SetViewIn {
	private String email;
	private String url;
	private String ip;
	private String token;
	private String name;
	private String phone;
	private String[] generalData;
	private Date created;
}
