package com.adaptaconsultoria.alc.objects.in;

import lombok.Data;

@Data
public class AuthPostIn {

	private String appToken;
	private String appPassword;
	private String userName;
	private String userPassword;
	private String ipAddress;

}
