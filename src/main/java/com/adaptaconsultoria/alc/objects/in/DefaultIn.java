package com.adaptaconsultoria.alc.objects.in;

import lombok.Data;

@Data
public class DefaultIn {
	private String token;
	private String ipAddress;

}
