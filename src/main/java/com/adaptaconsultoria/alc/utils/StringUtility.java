package com.adaptaconsultoria.alc.utils;

import java.util.HashMap;
import java.util.Map;

public class StringUtility {

	private StringUtility() {
	}

	public static String solveHTTP(String str) {
		return str.replace("http://", "").replace("https://", "");
	}
	
	public static String splitUrl(String url)
	{
	    String[] pairs = url.split("\\?");
	    return pairs[0];
	}
	
	public static Map<String, String> getMappedUrl(String query)  
	{  
	    String[] params = query.split("&");  
	    
	    Map<String, String> map = new HashMap<String, String>();  
	    for (String param : params)  
	    {  
	        String name = param.split("=")[0];  
	        String value = param.split("=")[1];  
	        map.put(name, value);  
	    }  
	    return map;  
	}
}
