package com.adaptaconsultoria.alc.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {

	public static Date toString(String format, String dateText) {
		DateFormat f = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = f.parse(dateText);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static LocalDate dateToLocalDate(Date date) {
		try {
			return  date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Date localDateToDate(LocalDate localDate) {
		try {
			return  Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		} catch (Exception e) {
			return null;
		}
	}
}
