package com.adaptaconsultoria.alc.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class JsonUtil {
	private JsonUtil() {
	}

	public static String toJson(Object object) {
		return new Gson().toJson(object);
	}

	public static Object fromJson(String str) {
		return new Gson().fromJson(str, Object.class);
	}

	public static String objToJsonString(Object obj) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static Object objToObj(Object source, Object dest) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.convertValue(source, dest.getClass());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}
