package com.adaptaconsultoria.alc.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Parameter;
import com.adaptaconsultoria.alc.objects.pojo.EmailConfig;

@Service
public interface EmailService {
	
	public Integer runGenerateQueueEmail();
	
	public void loadEmailsSetAndSend();
	
	public String send(EmailConfig emailConfig);
	
	public String toAlcPattern(String param);
	
	public String replaceWithParameters(List<Parameter> parameter, String email);
	
	public void sendMultiples(EmailConfig emailConfig, List<String> receivers);
}

