package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Action;
import com.adaptaconsultoria.alc.models.Config;
import com.adaptaconsultoria.alc.models.EmailSender;
import com.adaptaconsultoria.alc.objects.pojo.EmailConfig;
import com.adaptaconsultoria.alc.repositories.ActionRepository;
import com.adaptaconsultoria.alc.repositories.ConfigRepository;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {

	@Autowired
	private ActionRepository actionRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private ConfigRepository configRepository;

	@Override
	public Object prepareToSend(EmailSender emailSender) {
		try {
			Action action = actionRepository.findById(emailSender.getId()).get();
			Config config = configRepository.findTop1ByCompanyAndIsactiveTrue(action.getCompany()).get();
			
			EmailConfig emailConfig = new EmailConfig();
			emailConfig.setHost(config.getSmtpHost());
			emailConfig.setPort(config.getSmtpPort());
			emailConfig.setUsername(config.getSmtpUser());
			emailConfig.setPassword(config.getSmtpPassword());
			emailConfig.setSubject(action.getEmailSubject());
			emailConfig.setFrom(config.getSmtpSender());
			emailConfig.setMessage(replaceWithParameters(emailSender, action));

			emailService.sendMultiples(emailConfig, emailSender.getReceivers());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public String replaceWithParameters(EmailSender emailSender, Action action) {
		try {
			return emailService.replaceWithParameters(emailSender.getParameters(), action.getEmailText());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}