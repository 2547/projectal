package com.adaptaconsultoria.alc.services;

import java.util.HashMap;

import com.adaptaconsultoria.alc.models.Campaign;

public interface CampaignService {
	public HashMap<Object, Object> save(Campaign campaign);
	public void remove(Long id);
}
