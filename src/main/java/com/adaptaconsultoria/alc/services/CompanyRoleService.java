package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.CompanyRole;
import com.adaptaconsultoria.alc.repositories.CompanyRoleRepository;

@Service
public class CompanyRoleService {

	@Autowired private CompanyRoleRepository companyRoleRepository;

	public CompanyRole save(CompanyRole companyRole) {
		return this.companyRoleRepository.save(companyRole);
	}

	public void remove(CompanyRole companyRole) {
		this.companyRoleRepository.delete(companyRole);

	}

}