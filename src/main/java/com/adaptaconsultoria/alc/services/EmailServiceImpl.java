package com.adaptaconsultoria.alc.services;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Email;
import com.adaptaconsultoria.alc.models.Parameter;
import com.adaptaconsultoria.alc.objects.pojo.EmailConfig;
import com.adaptaconsultoria.alc.repositories.EmailRepository;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	private EmailRepository emailRepository;

	private JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	
	@Override
	public void loadEmailsSetAndSend() {
		List<Email> emails = emailRepository.findBySendFalse();

		for (Email email : emails) {
			try {
				EmailConfig emailConfig = new EmailConfig();
				emailConfig.setHost(email.getPhase().getCampaign().getSmtpHost());
				emailConfig.setPort(Integer.valueOf(email.getPhase().getCampaign().getSmtpPort()));
				emailConfig.setUsername(email.getPhase().getCampaign().getSmtpUser());
				emailConfig.setPassword(email.getPhase().getCampaign().getSmtpPassword());
				emailConfig.setSubject(email.getSubject());
				emailConfig.setFrom(email.getPhase().getCampaign().getSmtpSender());
				emailConfig.setTo(email.getEmail());
				
				send(emailConfig);
				
				email.setSend(true);
				email.setSendtime(new Date());
				emailRepository.save(email);
			} catch (Exception e) {
				e.printStackTrace();
				email.setSend(true);
				email.setFailed(true);
				email.setSendtime(new Date());
				emailRepository.save(email);
			}
		}
	}

	@Override
	public String send(EmailConfig emailConfig) {
		mailSender.setHost(emailConfig.getHost());
		mailSender.setPort(emailConfig.getPort());
		mailSender.setUsername(emailConfig.getUsername());
		mailSender.setPassword(emailConfig.getPassword());
		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
			String htmlMsg = emailConfig.getMessage();
			mimeMessage.setContent(htmlMsg, "text/html");
			helper.setTo(emailConfig.getTo());
			helper.setSubject(emailConfig.getSubject());
			helper.setFrom(emailConfig.getFrom());
		} catch (Exception e) {

		}

		try {
			mailSender.send(mimeMessage);
			return "Email enviado com sucesso para \"" + emailConfig.getTo() + "\", confira sua caixa de entrada!";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public Integer runGenerateQueueEmail() {
		return emailRepository.generateQueueEmail();
	}

	@Override
	public String toAlcPattern(String param) {
		try {
			return "[" + param + "]";
		} catch (Exception e) {
			e.getStackTrace();
		}
		return null;
	}

	@Override
	public String replaceWithParameters(List<Parameter> parameter, String email) {
		try {
			for (Parameter p : parameter) {
				email = email.replace(toAlcPattern(p.getName()), p.getValue());
				System.out.println(email);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return email;
	}

	@Override
	public void sendMultiples(EmailConfig emailConfig, List<String> receivers) {
		try {
			receivers.forEach(r -> {
				emailConfig.setTo(r);
				send(emailConfig);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
