package com.adaptaconsultoria.alc.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Phase;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;
import com.adaptaconsultoria.alc.utils.FLUC;

@Service
public class PhaseServiceImpl implements PhaseService {

	@Autowired
	private PhaseRepository phaseRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	public Phase save(Phase phase, Long campaignId) {
		phase.setName(FLUC.convert(phase.getName()));

		if (phase.getId() == null) {
			Optional<Campaign> campaignTempt = campaignRepository.findById(campaignId);

			try {
				if (campaignTempt.isPresent()) {
					phase.setCompany(campaignTempt.get().getCompany());
					phase.setCampaign(campaignTempt.get());
					phase.setCreatedBy(campaignTempt.get().getCreatedBy());
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				return null;
			}

		} else {
			Optional<Phase> phaseTempt = phaseRepository.findById(phase.getId());
			Phase phaseTemptObj = phaseTempt.get();

			phaseTemptObj.setName(phase.getName());
			phaseTemptObj.setDescription(phase.getDescription());
			phaseTemptObj.setSeqNo(phase.getSeqNo());
			phaseTemptObj.setUrl(phase.getUrl());
			phase = phaseTemptObj;
		}

		return phaseRepository.save(phase);
	}

	public void remove(Phase phase) {
		phase.setIsactive(false);
		phase.getActions().forEach( a -> a.setIsactive(false) );
		phaseRepository.save(phase);
	}

	@Override
	public Boolean isUniqueSeqNo(Long campaignId, Integer seqNo) {
		return phaseRepository.findByCampaignIdAndSeqNo(campaignId, seqNo).isPresent();
	}
}