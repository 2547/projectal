package com.adaptaconsultoria.alc.services;

import com.adaptaconsultoria.alc.models.Login;

public interface LoginService {
	public Login save(Login login, Long campaignId);
	public void remove(Login login);
}	
