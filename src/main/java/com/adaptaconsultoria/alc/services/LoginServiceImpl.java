package com.adaptaconsultoria.alc.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Login;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.repositories.LoginRepository;
import com.adaptaconsultoria.alc.utils.FLUC;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private CampaignRepository campaignRepository;

	public Login save(Login login, Long campaignId) {
		login.setName(FLUC.convert(login.getName()));

		if (login.getId() == null) {
			Optional<Campaign> campaignTempt = campaignRepository.findById(campaignId);

			try {
				if (campaignTempt.isPresent()) {
					login.setCompany(campaignTempt.get().getCompany());
					login.setCampaign(campaignTempt.get());
					login.setCreatedBy(campaignTempt.get().getCreatedBy());
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
				return null;
			}

		} else {
			Optional<Login> loginTempt = loginRepository.findById(login.getId());
			Login loginTemptObj = loginTempt.get();

			loginTemptObj.setName(login.getName());
			loginTemptObj.setLogin(login.getLogin());
			loginTemptObj.setEmail(login.getEmail());
			loginTemptObj.setCellphone(login.getCellphone());
			login = loginTemptObj;
		}

		return loginRepository.save(login);
	}

	public void remove(Login login) {
		loginRepository.delete(login);
	}
}