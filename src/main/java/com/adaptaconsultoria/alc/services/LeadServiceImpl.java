package com.adaptaconsultoria.alc.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.adaptaconsultoria.alc.models.Phase;
import com.adaptaconsultoria.alc.objects.in.SetViewIn;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;

@Service
public class LeadServiceImpl implements LeadService {

	@Autowired
	public ViewService viewService;

	@Autowired
	public PhaseRepository phaseRepository;

	@Override
	public HashMap<Object, Object> uploadLeads(MultipartFile file, Long phaseId) {
		HashMap<Object, Object> leads = new HashMap<>();
		try {
			Optional<Phase> opPhase = phaseRepository.findByIdAndIsactiveTrue(phaseId);
			opPhase.orElseThrow(() -> new Exception(("Campaign not found!")));
			leads = persist(fileToObj(file, opPhase.get().getUrl(), opPhase.get().getCampaign().getToken()));
			
			return leads;
		} catch (Exception e) {
			leads.put("message", e.getMessage());
			leads.put("success", false);
		}
		return leads;
	}

	@Override
	public List<SetViewIn> fileToObj(MultipartFile file, String url, String token) {
		BufferedReader br;
		String currentLine = new String();
		
		try {
			InputStream is = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			List<SetViewIn> setViewIns = new ArrayList<>();
			
			while ( (currentLine = br.readLine()) != null && !currentLine.isEmpty()) {
				
				String [] splittedCurrendLine = currentLine.split(";");
				SetViewIn setViewIn = new SetViewIn();
				
				if (splittedCurrendLine.length > 3) {
					throw new IllegalArgumentException("O arquivo não pode ser salvo, confira se o arquivo está no formato correto.");
				}
				
				if (splittedCurrendLine.length > 2) {
					int x = 0;
					setViewIn.setName(splittedCurrendLine[x++]);
					setViewIn.setEmail(splittedCurrendLine[x++]);
					setViewIn.setPhone(splittedCurrendLine[x++]);
				} else if (splittedCurrendLine.length > 1) {
					int y = 0;
					setViewIn.setName(splittedCurrendLine[y++]);
					setViewIn.setEmail(splittedCurrendLine[y++]);
				} else if (splittedCurrendLine.length == 1) {
					int z = 0;
					setViewIn.setName(splittedCurrendLine[z++]);
				} else {
					throw new IllegalArgumentException("O arquivo não pode ser salvo, confira se o arquivo está no formato correto.");
				}
				
				setViewIn.setUrl(Optional.of(url).orElse(null));
				setViewIn.setToken(Optional.of(token).orElse(null));
				setViewIns.add(setViewIn);
			}
			return setViewIns;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public HashMap<Object, Object> persist(List<SetViewIn> leads) {
		HashMap<Object, Object> map = new HashMap<>();
		try {
			if (!leads.isEmpty()) {
				leads.forEach(l -> {
					try {
						viewService.save(l);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				map.put("message", "Leads adicionados com sucesso!");
				map.put("success", true);
			} else {
				throw new EmptyStackException();
			}
			return map;
		} catch (Exception e) {
			map.put("message", "O arquivo não pode ser salvo, confira se o arquivo está no formato correto");
			map.put("success", false);
		}
		return map;
	}
}
