package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.Role;
import com.adaptaconsultoria.alc.repositories.RoleRepository;

@Service
public class RoleService {

	@Autowired private RoleRepository grupoRepository;

	public Role save(Role grupo, Company company) {
		grupo.setCompany(company);
		return this.grupoRepository.save(grupo);
	}

	public void remove(Role grupo) {
		this.grupoRepository.delete(grupo);
	}

}