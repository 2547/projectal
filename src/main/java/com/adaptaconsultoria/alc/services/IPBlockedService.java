package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.IPBlocked;
import com.adaptaconsultoria.alc.repositories.IPBlockedRepository;

@Service
public class IPBlockedService {

	@Autowired private IPBlockedRepository ipBlockedRepository;
	public Boolean isIpAddressBlocked(String ipAddress) {
		IPBlocked ipBlocked = ipBlockedRepository.findTop1ByIpAddressAndIsactiveTrue(ipAddress);
		if (ipBlocked != null) {
			return true;
		}
		return false;
	}

}