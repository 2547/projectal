package com.adaptaconsultoria.alc.services;

import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Action;

@Service
public interface ActionService {
	public Action save(Action action, Long campaignId);
	public void remove(Long id);
}	
