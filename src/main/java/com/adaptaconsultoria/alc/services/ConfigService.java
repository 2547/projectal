package com.adaptaconsultoria.alc.services;

import java.util.HashMap;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.Config;

public interface ConfigService {
	// public Object save(Config config);
	public HashMap<Object, Object> save(Config config);

	public Boolean remove(Long id);

	public Object list(Company company);
	
}