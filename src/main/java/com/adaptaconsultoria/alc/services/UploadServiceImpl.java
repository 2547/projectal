package com.adaptaconsultoria.alc.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadServiceImpl implements UploadService {
	
	private static String UPLOADED_FOLDER = "/home/adapta/Documentos/";
	
	@Override
	public Boolean singleFileUpload(MultipartFile file) {
		if (file.isEmpty()) {
			return false;
		}

		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);
			
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
