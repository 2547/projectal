package com.adaptaconsultoria.alc.services;

import java.util.List;

public interface DefaultAlcParameters {
	List<String> parameters();
}
