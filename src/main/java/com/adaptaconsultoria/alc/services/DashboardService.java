package com.adaptaconsultoria.alc.services;

import java.util.List;

import com.adaptaconsultoria.alc.models.Dashboard;
import com.adaptaconsultoria.alc.models.Lead;
import com.adaptaconsultoria.alc.models.LineChartCapturedUser;
import com.adaptaconsultoria.alc.models.LineChartCapturedUserDate;
import com.adaptaconsultoria.alc.models.PieChartVisitedUsersByPhase;

public interface DashboardService {
	public List<Dashboard> getDashboardsByUserId(Long userId); 
	public List<Lead> getLeadsByCampaignId(Long campaignId); 
	public List<LineChartCapturedUser> getLineChartsCapturedUsersByCampaignId(Long campaignId); 
	public List<LineChartCapturedUserDate> getLineChartsCapturedUsersByCampaignIdAndDate(Long campaignId, String date);
	public List<PieChartVisitedUsersByPhase> getPieChartByCampaignId(Long campaignId);
}
