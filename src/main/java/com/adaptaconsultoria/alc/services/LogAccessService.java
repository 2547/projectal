package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.LogAccess;
import com.adaptaconsultoria.alc.repositories.LogAccessRepository;

@Service
public class LogAccessService {

	@Autowired private LogAccessRepository logAcessoRepository;
	
	public LogAccess save(LogAccess logAcesso ) {
		return logAcessoRepository.save(logAcesso);
	}

}
