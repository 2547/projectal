package com.adaptaconsultoria.alc.services;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.CompanyRole;
import com.adaptaconsultoria.alc.models.LogAccess;
import com.adaptaconsultoria.alc.models.User;
import com.adaptaconsultoria.alc.repositories.CompanyRoleRepository;
import com.adaptaconsultoria.alc.repositories.LogAccessRepository;
import com.adaptaconsultoria.alc.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private CompanyRoleRepository companyRoleRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private LogAccessRepository logAccessRepository;
	@Autowired
	private TokenService tokenService;

	public User salvar(User usuario, Company company) throws Exception {

		if (usuario.getCompany() == null && usuario.getId() != null) {
			if (company != null) {
				usuario.setCompany(company);
			}
		}

		User usuarioExistente = userRepository.findTop1ByLoginOrderByLogin(usuario.getLogin());
		if (usuarioExistente != null && !usuarioExistente.equals(usuario)
				&& StringUtils.isNotBlank(usuarioExistente.getLogin())) {
			throw new Exception("Login já existente, informe um novo login!");
		}

		if (usuario.getCompanyRole() == null && usuario.getId() != null) {
			List<CompanyRole> empresaGrupos = companyRoleRepository.findByUser(usuario);
			if (empresaGrupos.size() > 0) {
				usuario.setCompanyRole(empresaGrupos.get(0));
			}
		}

		return userRepository.save(usuario);
	}

	public User salvarInvestidor(User usuario, Company company) throws Exception {

		if (usuario.getCompany() == null && usuario.getId() != null) {
			if (company != null) {
				usuario.setCompany(company);
			}
		}

		User usuarioExistente = userRepository.findTop1ByLoginOrderByLogin(usuario.getLogin());
		if (usuarioExistente != null && !usuarioExistente.equals(usuario)
				&& StringUtils.isNotBlank(usuarioExistente.getLogin())) {
			throw new Exception("Login já existente, informe um novo login!");
		}

		if (usuario.getCompanyRole() == null && usuario.getId() != null) {
			List<CompanyRole> empresaGrupos = companyRoleRepository.findByUser(usuario);
			if (empresaGrupos.size() > 0) {
				usuario.setCompanyRole(empresaGrupos.get(0));
			}
		}

		return userRepository.save(usuario);
	}

	public User checkLogin(Company company, String userLogin, String userPassword) throws Exception {
		User user = userRepository.findTop1ByCompanyAndLoginAndStatusTrueOrderByLogin(company, userLogin);
		if (user == null) {
			throw new Exception("User not found!");
		}
		if (!tokenService.compare(user.getPassword(), userPassword)) {
			throw new Exception("User not found!");
		}
		return user;
	}

	public User saveWithoutCompanyRole(User usuario) {
		return userRepository.save(usuario);
	}

	public void remove(User usuario) {
		userRepository.delete(usuario);
	}

	public LogAccess saveLogAccess(LogAccess log) {
		return logAccessRepository.save(log);
	}

	public User getCurrentUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof User) {
			return (User) principal;
		} else {
			return null;
		}
	}
}