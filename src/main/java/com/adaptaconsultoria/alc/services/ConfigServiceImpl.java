package com.adaptaconsultoria.alc.services;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.Config;
import com.adaptaconsultoria.alc.repositories.ConfigRepository;

@Service
public class ConfigServiceImpl implements ConfigService {
	
	@Autowired
	private ConfigRepository configRepository;
	
	@Autowired
	private UserService userService;

	@Override
	public HashMap<Object, Object> save(Config config) {
		HashMap<Object, Object> map = new HashMap<>();
		Boolean isPresent = configRepository.findTop1ByCompanyAndIsactiveTrue(userService.getCurrentUser().getCompany()).isPresent();
		try {
			if (isPresent && config.getId() == null) throw new Exception();
			config.setCompany( userService.getCurrentUser().getCompany() );
			config = configRepository.save(config);
			map.put("sucesso", true);
			map.put("message", "Registro enviado com sucesso!");
			map.put("obj", config);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", config);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	@Override
	public Boolean remove(Long id) {
		try {
			configRepository.delete( configRepository.findById(id).get() );
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Object list(Company company) {
		try {
			return configRepository.findByCompany(company);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}