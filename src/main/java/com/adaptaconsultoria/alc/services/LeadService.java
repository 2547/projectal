package com.adaptaconsultoria.alc.services;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.adaptaconsultoria.alc.objects.in.SetViewIn;

public interface LeadService {
	public HashMap<Object, Object> uploadLeads(MultipartFile file, Long phaseId);
	public List<SetViewIn> fileToObj(MultipartFile file, String url, String token);
	public HashMap<Object, Object> persist(List<SetViewIn> leads);
}	
