package com.adaptaconsultoria.alc.services;

import com.adaptaconsultoria.alc.models.EmailSender;

public interface EmailSenderService {
	public Object prepareToSend(EmailSender emailSender);
}