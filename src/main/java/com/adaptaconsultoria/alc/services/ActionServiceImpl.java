package com.adaptaconsultoria.alc.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Action;
import com.adaptaconsultoria.alc.models.Phase;
import com.adaptaconsultoria.alc.repositories.ActionRepository;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;
import com.adaptaconsultoria.alc.utils.FLUC;

@Service
public class ActionServiceImpl implements ActionService {

	@Autowired
	private ActionRepository actionRepository;

	@Autowired
	private PhaseRepository phaseRepository;

	@Autowired
	private UserService userService;

	public Action save(Action action, Long phaseId) {
		action.setEmailSubject(FLUC.convert(action.getEmailSubject()));
		if (action.getId() == null) {
			if (phaseId == null) {
				saveAlone(action);
			} else {
				Optional<Phase> phaseTempt = phaseRepository.findById(phaseId);
				try {
					if (phaseTempt.isPresent()) {
						action.setCompany(phaseTempt.get().getCompany());
						action.setPhase(phaseTempt.get());
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
					return null;
				}
			}
		} else {
			Optional<Action> actionTempt = actionRepository.findById(action.getId());
			Action actionTemptObj = actionTempt.get();

			actionTemptObj.setSeqNo(action.getSeqNo());
			actionTemptObj.setEmailSubject(action.getEmailSubject());
			actionTemptObj.setEmailText(action.getEmailText());
			actionTemptObj.setSmsText(action.getSmsText());
			actionTemptObj.setDeadline(action.getDeadline());
			actionTemptObj.setDeadlineType(action.getDeadlineTypeT());
			action = actionTemptObj;
		}
		return actionRepository.save(action);
	}

	public void saveAlone(Action action) {
		action.setCompany(userService.getCurrentUser().getCompany());
	}

	public void remove(Long id) {
		actionRepository.remove(id);
	}
}