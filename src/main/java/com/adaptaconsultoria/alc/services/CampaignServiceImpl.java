package com.adaptaconsultoria.alc.services;

import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Config;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.repositories.ConfigRepository;
import com.adaptaconsultoria.alc.repositories.LoginRepository;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;
import com.adaptaconsultoria.alc.utils.FLUC;

@Service
public class CampaignServiceImpl implements CampaignService {
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	@Autowired
	private ConfigRepository configRepository;
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private PhaseRepository phaseRepository;

	@Override
	public HashMap<Object, Object> save(Campaign campaign) {
		HashMap<Object, Object> map = new HashMap<>();
		Boolean isEditing = true;
		campaign.setName(FLUC.convert(campaign.getName()));
		if (campaign.getId() == null) {
			campaign.setToken("1");
			campaign = campaignRepository.save(campaign);
			campaign.setToken("alc-" + campaign.getCompany().getId() + "-" + campaign.getId());
			isEditing = false;
		}
		if (campaign.getDefaultsmtp()) {
			Optional<Config> op = configRepository.findTop1ByCompanyAndIsactiveTrue(campaign.getCompany());
			if (op.isPresent()) {
				campaign.setSmtpHost(op.get().getSmtpHost());
				campaign.setSmtpPassword(op.get().getSmtpPassword());
				campaign.setSmtpPort( String.valueOf(op.get().getSmtpPort()) );
				campaign.setSmtpSender(op.get().getSmtpSender());
				campaign.setSmtpUser(op.get().getSmtpUser());
			}
		}
		
		campaign = campaignRepository.save(campaign);
		map.put("campaign", campaign);
		map.put("isEditing", isEditing);
		return map;
	}
	
	public void foo (Config c) {
		
	}

	@Override
	public void remove(Long id) {
		try {
			
			campaignRepository.findByIdAndIsactiveTrue(id).ifPresent(c -> {
				
				loginRepository.findOpByCampaignAndIsactiveTrue(c).ifPresent(ll -> {
					ll.forEach(l -> {
						l.get().setIsactive(false);
					});
				});
				
				phaseRepository.findOpByCampaignAndIsactiveTrue(c).ifPresent(pl -> {
					pl.forEach(p -> {
						p.get().setIsactive(false);
					});
				});

				
				c.setIsactive(false);
				campaignRepository.save(c);
			});
		} catch (Exception e) {
			throw e;
		}
	}
}
