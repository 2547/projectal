package com.adaptaconsultoria.alc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Dashboard;
import com.adaptaconsultoria.alc.models.Lead;
import com.adaptaconsultoria.alc.models.LineChartCapturedUser;
import com.adaptaconsultoria.alc.models.LineChartCapturedUserDate;
import com.adaptaconsultoria.alc.models.PieChartVisitedUsersByPhase;
import com.adaptaconsultoria.alc.repositories.DashboardRepository;
import com.adaptaconsultoria.alc.repositories.LeadRepository;
import com.adaptaconsultoria.alc.repositories.LineChartCapturedUserDateRepository;
import com.adaptaconsultoria.alc.repositories.LineChartCapturedUserRepository;
import com.adaptaconsultoria.alc.repositories.PieChartVisitedUsersByPhaseRepository;
import com.adaptaconsultoria.alc.utils.DateUtil;

@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private DashboardRepository dashboardRepository;
	
	@Autowired
	private LeadRepository leadRepository;
	
	@Autowired
	private LineChartCapturedUserRepository lineChartCapturedUserRepository;
	
	@Autowired
	private LineChartCapturedUserDateRepository lineChartCapturedUserDateRepository;
	
	@Autowired
	private PieChartVisitedUsersByPhaseRepository pieChartVisitedUsersByPhaseRepository;

	@Override
	public List<Dashboard> getDashboardsByUserId(Long userId) {
		try {
			return dashboardRepository.getDashboardByUserId(userId);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Lead> getLeadsByCampaignId(Long campaignId) {
		try {
			return leadRepository.getLeadByCompanyId(campaignId);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<LineChartCapturedUser> getLineChartsCapturedUsersByCampaignId(Long campaignId) {
		try {
			return lineChartCapturedUserRepository.getLineChartsCapturedUsersByCampaignId(campaignId);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<LineChartCapturedUserDate> getLineChartsCapturedUsersByCampaignIdAndDate(Long campaignId, String date) {
		try {
			return lineChartCapturedUserDateRepository.getLineChartsCapturedUsersByCampaignIdAndDate(campaignId, DateUtil.toString("dd/MM/yyyy", date));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<PieChartVisitedUsersByPhase> getPieChartByCampaignId(Long campaignId) {
		try {
			return pieChartVisitedUsersByPhaseRepository.getPieChartByCampaignId(campaignId);
		} catch (Exception e) {
			return null;
		}
	}
}
