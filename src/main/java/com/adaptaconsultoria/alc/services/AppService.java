package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.App;
import com.adaptaconsultoria.alc.repositories.AppRepository;

@Service
public class AppService {

	@Autowired private AppRepository appRepository;

	public App checkAppLogin(String appToken, String appPassword) throws Exception {
		App app = appRepository.findTop1ByTokenAndIsactiveTrue(appToken);
		if (app == null) {
			throw new Exception("App not found!");
		}
		if (!app.getPassword().equals(appPassword)) {
			throw new Exception("App not found!");
		}
		return app;
	}

}