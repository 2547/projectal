package com.adaptaconsultoria.alc.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.App;
import com.adaptaconsultoria.alc.models.AppSession;
import com.adaptaconsultoria.alc.models.User;
import com.adaptaconsultoria.alc.objects.pojo.Token;
import com.adaptaconsultoria.alc.repositories.AppSessionRepository;

@Service
public class TokenService {

	@Autowired AppSessionRepository appSessionRepository;
	@Autowired AppSessionService appSessionService;

	public String getAccessToken(App app, User user, String... parameters) throws Exception {
		String tokenDecrypted = "";

		tokenDecrypted += app.getToken() + "||||" + app.getPassword() + "||||"; 

		if (user != null) {
			tokenDecrypted += user.getLogin() + "||||" + user.getPassword() + "||||";
		}

		for (String param : parameters) {
			tokenDecrypted += param + "||||";
		}

		tokenDecrypted += (new Date()).getTime();

		return encryptToken(tokenDecrypted);
	}

	public String getAccessAppToken(String appToken, String appPassword, String ipAddress) throws Exception {
		String tokenDecrypted = appToken + "||||" + appPassword + "||||" + ipAddress + "||||" + (new Date()).getTime();
		String tokenEncrypted = encryptToken(tokenDecrypted);
		return tokenEncrypted;
	}

	public Token checkToken(String token, String ipAddress, String serviceName, String args, boolean transacional, Boolean useIpAddress) throws Exception {

		Token t = new Token();

		if (StringUtils.isBlank(token)) {
			throw new Exception("Token is required!");
		}

		if (StringUtils.isBlank(ipAddress)) {
			throw new Exception("IpAddress is required!");
		}

		AppSession appSession = null;
		if (useIpAddress) {
			appSession = appSessionRepository.findTop1ByTokenAndIpAddressAndValidtoGreaterThanAndIsactiveTrue(token, ipAddress, new Date());
		}
		if (appSession == null) {
			throw new Exception("Invalid token!");
		}

		try {
			if (transacional) {
				if (appSession.getUser() == null) {
					throw new Exception("Invalid token to current transaction!");
				}
			}
		} catch (Exception e) {
			t.setError(e.getMessage());
		}

		appSession.setIsactive(false);
		appSession = appSessionService.save(appSession);

		// Ativa nova sessão
		AppSession appSessionNew = appSession.clone();
		appSessionNew.setId(null);
		appSessionNew.setIsactive(true);
		appSessionNew.setCreated(new Date());
		appSessionNew.setToken(encryptToken(token).substring(0, 32));
		appSessionNew.setValidto(new Date((new Date()).getTime() + (60000 * 15)));
		appSessionNew.setServicename(serviceName);
		appSessionNew.setServiceargs(args);
		appSessionNew = appSessionService.save(appSessionNew);

		t.setToken(appSessionNew.getToken());

		return t;
	}

	public Boolean compare(String encrypted, String compareWith) {
		try {
			return encrypted.toUpperCase().equals(encryptToken(compareWith).toUpperCase());
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	public String encrypt(String encrypt) throws Exception {
		return encryptToken(encrypt);
	}
	private String encryptToken(String tokenDecrypted) throws Exception {
		String retorno = tokenDecrypted;

		try {
			MessageDigest md;
			byte[] token;
			md = MessageDigest.getInstance("MD5");
			md.update(retorno.getBytes());
			token = md.digest();

		   StringBuilder sb = new StringBuilder();
		   for (int i = 0; i < token.length; i++) {
		       int parteAlta = ((token[i] >> 4) & 0xf) << 4;
		       int parteBaixa = token[i] & 0xf;
		       if (parteAlta == 0) sb.append('0');
		       sb.append(Integer.toHexString(parteAlta | parteBaixa));
		   }
		   retorno = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("Token could not be created!");
		}

		try {
			MessageDigest md;
			byte[] token;
			md = MessageDigest.getInstance("SHA-256");
			md.update(retorno.getBytes());
			token = md.digest();

		   StringBuilder sb = new StringBuilder();
		   for (int i = 0; i < token.length; i++) {
		       int parteAlta = ((token[i] >> 4) & 0xf) << 4;
		       int parteBaixa = token[i] & 0xf;
		       if (parteAlta == 0) sb.append('0');
		       sb.append(Integer.toHexString(parteAlta | parteBaixa));
		   }
		   retorno = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("Token could not be created!");
		}

		return retorno.toUpperCase();
	}

}