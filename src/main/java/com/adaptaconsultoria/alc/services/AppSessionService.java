package com.adaptaconsultoria.alc.services;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.App;
import com.adaptaconsultoria.alc.models.AppSession;
import com.adaptaconsultoria.alc.models.User;
import com.adaptaconsultoria.alc.repositories.AppSessionRepository;

@Service
public class AppSessionService {

	@Autowired private TokenService tokenService;
	@Autowired private AppSessionRepository appSessionRepository;

	public AppSession save(AppSession appSession) {
		return appSessionRepository.save(appSession);
	}

	public AppSession saveToken(App app, User user, String ipAddress, String serviceName) throws Exception {
		String token = null;
		try {
			token = tokenService.getAccessToken(app, user, ipAddress);
		} catch (Exception e) {
			throw new Exception("Token cannot be created!");
		}

		if (StringUtils.isBlank(token)) {
			throw new Exception("Token cannot be created!");
		}

		AppSession appSession = new AppSession();
		appSession.setCompany(app.getCompany());
		appSession.setApp(app);
		appSession.setUser(user);
		appSession.setIpAddress(ipAddress);
		appSession.setServicename(serviceName);

		appSession.setToken(token.substring(0,32));
		appSession.setValidto(new Date((new Date()).getTime() + (60000 * 15)));
		appSession.setIsactive(true);

		try {
			appSession = save(appSession);
		} catch (Exception e) {
			throw new Exception("Token cannot be created!");
		}

		return appSession;
	}
}