package com.adaptaconsultoria.alc.services;

import com.adaptaconsultoria.alc.models.Phase;

public interface PhaseService {
	public Phase save(Phase phase, Long campaignId);
	public void remove(Phase phase);
	public Boolean isUniqueSeqNo(Long phaseId, Integer seqNo);
}	
