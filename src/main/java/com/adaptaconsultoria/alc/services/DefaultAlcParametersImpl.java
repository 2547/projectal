package com.adaptaconsultoria.alc.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class DefaultAlcParametersImpl implements DefaultAlcParameters {

	@Autowired
	private Environment env;

	@Override
	public List<String> parameters() {
		try {
			return new ArrayList<>(
					Arrays.asList(
						env.getProperty("default.parameters.username"), 
						env.getProperty("default.parameters.email"), 
						env.getProperty("default.parameters.nextphase")
					)
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
