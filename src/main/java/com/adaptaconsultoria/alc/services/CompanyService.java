package com.adaptaconsultoria.alc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.repositories.CompanyRepository;

@Service
public class CompanyService {

	@Autowired private CompanyRepository companyRepository;

	public Company save(Company company) {
		return companyRepository.save(company);
	}

	public void remove(Company company) {
		companyRepository.delete(company);
	}

}