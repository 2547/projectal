package com.adaptaconsultoria.alc.services;

import org.springframework.web.multipart.MultipartFile;

public interface UploadService {
	public Boolean singleFileUpload(MultipartFile file);
}
