package com.adaptaconsultoria.alc.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.Phase;
import com.adaptaconsultoria.alc.models.View;
import com.adaptaconsultoria.alc.models.ViewParam;
import com.adaptaconsultoria.alc.objects.in.SetViewIn;
import com.adaptaconsultoria.alc.objects.out.SetViewOut;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.repositories.CompanyRepository;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;
import com.adaptaconsultoria.alc.repositories.ViewRepository;
import com.adaptaconsultoria.alc.utils.StringUtility;

@Service
public class ViewService {

	@Autowired
	CampaignRepository campaignRepository;

	@Autowired
	PhaseRepository phaseRepository;

	@Autowired
	private ViewRepository viewRepository;

	@Autowired
	CompanyRepository companyRepository;

	Logger logger = LoggerFactory.getLogger(ViewService.class);

	public View save(SetViewIn viewIn) throws Exception {
		String[] token = viewIn.getToken().split("-");
		String[] urlParam = viewIn.getUrl().split("\\?");
		Company company = new Company();

		try {
			company = companyRepository.findById(Long.parseLong(token[1])).orElseThrow(() -> new Exception("Company do not exists"));
		} catch (NumberFormatException e1) {
			logger.warn(e1.getLocalizedMessage());
		} catch (Exception e1) {
			logger.warn(e1.getLocalizedMessage());
		}

		Optional<Campaign> optional = campaignRepository.findByIdAndCompany(Long.parseLong(token[2]), company);
		optional.orElseThrow(() -> new Exception(("Campaign not found!")));
		Campaign campaign = optional.get();

		// creating the view object
		View view = new View();
		view.setCompany(company);
		
		view.setEmail(viewIn.getEmail());
		
		view.setIpAddress(viewIn.getIp());
		view.setName(viewIn.getName());
		view.setUrl(urlParam[0]);
		view.setPhone(viewIn.getPhone());
		// view.setCreated(viewIn.getCreated());
		Phase phase = phaseRepository.findTop1ByUrlAndCampaign(StringUtility.solveHTTP(view.getUrl()), campaign);

		if (phase == null)
			throw new Exception("Phase do not exists!");

		view.setPhase(phase);

		try {
			Map<String, String> paramValue = StringUtility.getMappedUrl(urlParam[1]);
			ViewParam viewParam = new ViewParam();

			for (Map.Entry<String, String> entry : paramValue.entrySet()) {
				System.out.println(entry.getKey() + "/" + entry.getValue());
				viewParam.setView(view);
				viewParam.setCompany(company);
				viewParam.setName(entry.getKey());
				viewParam.setValue(entry.getValue());
				view.getViewParams().add(viewParam);
			}

		} catch (Exception e) {

		}
		return viewRepository.save(view);
	}

	public String getToken(String input) {
		return input.split("-")[2];
	}

	public void remove(View view) {
		viewRepository.delete(view);
	}

	public SetViewOut getSetViewOut(View view) {
		SetViewOut setViewOut = new SetViewOut();
		try {
			setViewOut.setEmail( view.getEmail());
		} catch (Exception e) {
		}
		try {
			setViewOut.setNextPhaseUrl(getNextPhaseUrl(view));
		} catch (Exception e) {
		}
		try {
			setViewOut.setParam(getParamNoEmail(view.getViewParams()));
		} catch (Exception e) {
		}
		return setViewOut;
	}

	public String getParamNoEmail(List<ViewParam> viewParams) {
		for (ViewParam viewParam : viewParams) {
			if (!viewParam.getValue().equals("email")) {
				return viewParam.getValue();
			}
		}
		return null;
	}

	public String getNextPhaseUrl(View view) {
		Optional<Phase> currentPhase = phaseRepository.findById(view.getPhase().getId());
		Integer nextPhaseSeg = currentPhase.get().getSeqNo().intValue() + 1;
		Optional<Phase> nextPhase = phaseRepository.findByCompanyAndSeqNoAndCampaign(view.getCompany().getId(), nextPhaseSeg, currentPhase.get().getCampaign().getId());
		return nextPhase.get().getUrl();
	}

	public boolean isValidEmail(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}
}