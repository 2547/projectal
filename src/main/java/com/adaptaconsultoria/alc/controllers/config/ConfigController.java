package com.adaptaconsultoria.alc.controllers.config;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.alc.models.Config;
import com.adaptaconsultoria.alc.repositories.ConfigRepository;
import com.adaptaconsultoria.alc.services.ConfigService;
import com.adaptaconsultoria.alc.services.UserService;

@Controller
@RequestMapping("config")
public class ConfigController {

	@Autowired
	private UserService userService;

	@Autowired
	private ConfigService configService;
	
	@Autowired
	private ConfigRepository configRepository;

	@GetMapping(value = "/list")
	public ModelAndView config(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView(request.getServletPath());
		modelAndView.addObject("page", "Configuração");
		modelAndView.addObject("title", "Lista de Configurações");
		modelAndView.addObject("subTitle", "Cadastro de Configurações");
		modelAndView.addObject("caption", "Cadastre e atualize Configurações");
		modelAndView.addObject("tableTitle", "Tabela de Configurações cadastradas");
		modelAndView.addObject("tableId", "config-table");
		modelAndView.addObject("js", "config-list.js");
		modelAndView.addObject("menuId", "config");
		return modelAndView;
	}

	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getActions() {
		return ResponseEntity.ok(configService.list(userService.getCurrentUser().getCompany()));
	}

	@GetMapping(value = "/register")
	public ModelAndView actionRegister(HttpSession session, Optional<Long> id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView(request.getServletPath());
		modelAndView.addObject("page", "Configuração");
		modelAndView.addObject("title", "Cadastrar de Configurações");
		modelAndView.addObject("subTitle", "Cadastrar de Configurações");
		modelAndView.addObject("caption", "Cadastre e atualize Configurações");
		modelAndView.addObject("tableTitle", "Tabela de Configurações cadastradas");
		modelAndView.addObject("tableId", "config-table");
		modelAndView.addObject("modelAttribute", "config");
		modelAndView.addObject("formId", "config-form");
		modelAndView.addObject("js", "config-register.js");
		Object obj = configRepository.findById(id.orElse( new Long(-1) )).orElse(new Config());
		modelAndView.addObject("config", obj);
		modelAndView.addObject("menuId", "config");
		return modelAndView;
	}
	
	@PostMapping(value = "/save")
	public ResponseEntity<?> save(Config config) {
		try {
			return ResponseEntity.ok(configService.save(config));
		} catch (Exception e) {
			return new ResponseEntity<>("Error", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping(value = "/remove")
	public ResponseEntity<?> remove(Long id) {
		try {
			return ResponseEntity.ok(configService.remove(id));
		} catch (Exception e) {
			return new ResponseEntity<>("Error", HttpStatus.BAD_REQUEST);
		}
	}
}
