package com.adaptaconsultoria.alc.controllers.rest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.alc.models.View;
import com.adaptaconsultoria.alc.objects.in.SetViewIn;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.repositories.CompanyRepository;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;
import com.adaptaconsultoria.alc.services.ViewService;

@RestController
public class SetViewController {

	@Autowired
	PhaseRepository phaseRepository;

	@Autowired
	ViewService viewService;
	
	@Autowired
	CampaignRepository campaignRepository;
	
	@Autowired
	CompanyRepository companyRepository;
 
	@SuppressWarnings("unused")
	@GetMapping("/setview")
	@CrossOrigin
	public ResponseEntity<?> getView(SetViewIn viewIn, HttpServletResponse response) {
		
		View view = new View();

		try {
			view = viewService.save(viewIn);
		} catch (Exception e) {
			return ResponseEntity.ok(e.getLocalizedMessage());
		}
		return ResponseEntity.ok("Okay!");
	}

	@PostMapping("/setview")
	@CrossOrigin
	public ResponseEntity<?> postView(SetViewIn viewIn, HttpServletResponse response) {

		View view = new View();
		try {
			view = viewService.save(viewIn);
		} catch (Exception e) {
			return ResponseEntity.ok(e);
		}
		return ResponseEntity.ok(viewService.getSetViewOut(view));
	}
	
	@PostMapping("/campaings")
	public  ResponseEntity<?> getCampaigns(String companyId) {
		try {
			return ResponseEntity.ok(campaignRepository.findByCompanyAndIsactiveTrue(companyRepository.getOne(Long.valueOf(companyId))));
		} catch (Exception e) {
			return null;
		}
	}
}
