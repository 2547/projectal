package com.adaptaconsultoria.alc.controllers.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.adaptaconsultoria.alc.objects.in.DefaultIn;
import com.adaptaconsultoria.alc.objects.out.DefaultOut;
import com.adaptaconsultoria.alc.objects.out.error.DefaultError;
import com.adaptaconsultoria.alc.objects.pojo.Token;
import com.adaptaconsultoria.alc.services.TokenService;
import com.adaptaconsultoria.alc.utils.JsonUtil;

public class DefaultRestController {

	@Autowired private TokenService tokenService;

	public Boolean validarCampos(DefaultIn in, DefaultOut out, String uri) throws Exception {
		return validarCampos(in, out, uri, true);
	}

	public Boolean validarCampos(DefaultIn in, DefaultOut out, String uri, Boolean transacional) throws Exception {
		return validarCampos(in, out, uri, transacional, true);
	}

	public Boolean validarCampos(DefaultIn in, DefaultOut out, String uri, Boolean transacional, Boolean useIpAddress) throws Exception {
		Boolean ok = true;
		DefaultError error = new DefaultError();
		if (StringUtils.isBlank(in.getToken())) {
			error.setError("token is required!");
			ok = false;
		} else {
			Token token = tokenService.checkToken(in.getToken(), in.getIpAddress(), uri, JsonUtil.toJson(in), transacional, useIpAddress);
			if (token == null) {
				throw new Exception("Token could not be renewed!");
			}
			out.setToken(token.getToken());
			if (StringUtils.isNotBlank(token.getError())) {
				ok = false;
				error.setError(token.getError());
			}
		}
		if (!ok) {
			out.setError(error);
		}
		out.setHasError(!ok);
		return ok;
	}
}
