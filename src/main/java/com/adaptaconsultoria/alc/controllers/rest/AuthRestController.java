package com.adaptaconsultoria.alc.controllers.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.alc.models.AppSession;
import com.adaptaconsultoria.alc.objects.in.AuthPostIn;
import com.adaptaconsultoria.alc.objects.out.AuthPostOut;
import com.adaptaconsultoria.alc.objects.out.error.AuthPostError;
import com.adaptaconsultoria.alc.services.AuthService;

@RestController
@RequestMapping("unrestricted")
public class AuthRestController {

	@Autowired
	private AuthService authService;

	@PostMapping(value = "/auth")
	public AuthPostOut post(@RequestBody AuthPostIn in) {
		AuthPostOut out = new AuthPostOut();
		try {
			if (this.validarCampos(in, out)) {
				AppSession appSession = authService.getAccessToken(in, "/unrestricted");
				if (appSession == null) {
					AuthPostError error = new AuthPostError();
					error.setError("Token could not be created!");
					out.setError(error);
				} else {
					out.setToken(appSession.getToken());
				}
			}
		} catch (Exception e) {
			AuthPostError error = new AuthPostError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	private Boolean validarCampos(AuthPostIn in, AuthPostOut out) {
		Boolean ok = true;
		AuthPostError error = new AuthPostError();

		if (StringUtils.isBlank(in.getAppToken())) {
			error.setAppToken("appToken is required!");
			ok = false;
		}

		if (StringUtils.isBlank(in.getAppPassword())) {
			error.setAppPassword("appPassword is required!");
			ok = false;
		}

		if (StringUtils.isBlank(in.getIpAddress())) {
			error.setUserPassword("ipAddress is required!");
			ok = false;
		}

		if (!ok) {
			out.setError(error);
		}

		out.setHasError(!ok);
		return ok;
	}
}