package com.adaptaconsultoria.alc.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.alc.models.EmailSender;
import com.adaptaconsultoria.alc.objects.out.DefaultOut;
import com.adaptaconsultoria.alc.objects.out.error.DefaultError;
import com.adaptaconsultoria.alc.repositories.AppSessionRepository;
import com.adaptaconsultoria.alc.services.EmailSenderService;

@RequestMapping("unrestricted")
@RestController
public class EmailSenderController extends DefaultRestController {
	
	@Autowired
	private EmailSenderService emailSenderService;
	
	@Autowired
	private AppSessionRepository appSessionRepository;

	@PostMapping(value = "/sender")
	public Object emailSender(@RequestBody EmailSender emailSender) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validarCampos(emailSender, out, "/sender", false)) {
				appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				emailSenderService.prepareToSend(emailSender);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return ResponseEntity.ok(out);
	}
}