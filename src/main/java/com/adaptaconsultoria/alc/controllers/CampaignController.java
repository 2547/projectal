package com.adaptaconsultoria.alc.controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.adaptaconsultoria.alc.converters.Converter;
import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.Login;
import com.adaptaconsultoria.alc.models.Phase;
import com.adaptaconsultoria.alc.models.User;
import com.adaptaconsultoria.alc.objects.pojo.EmailConfig;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.repositories.CompanyRepository;
import com.adaptaconsultoria.alc.repositories.LoginRepository;
import com.adaptaconsultoria.alc.repositories.PhaseRepository;
import com.adaptaconsultoria.alc.repositories.UserRepository;
import com.adaptaconsultoria.alc.services.CampaignService;
import com.adaptaconsultoria.alc.services.EmailService;
import com.adaptaconsultoria.alc.services.LeadService;
import com.adaptaconsultoria.alc.services.LoginService;
import com.adaptaconsultoria.alc.services.PhaseService;
import com.adaptaconsultoria.alc.services.UserService;

@Controller
public class CampaignController {

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private PhaseService phaseService;

	@Autowired
	private PhaseRepository phaseRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private LoginService loginService;

	@Autowired
	private UserService userService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private LeadService leadService;
	
	@Autowired
	private UserRepository userRepository;

	private static final String path = "/campaign";

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Company.class, "company", new Converter(companyRepository));
		binder.registerCustomEditor(User.class, "createdBy", new Converter(userRepository));
	}

	@GetMapping(value = "campaignlist")
	public ModelAndView campaign(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView(path + request.getServletPath());
		modelAndView.addObject("page", "Campanha");
		modelAndView.addObject("title", "Lista de Campanhas");
		modelAndView.addObject("subTitle", "Cadastro de Campanhas");
		modelAndView.addObject("caption", "Cadastre e atualize Campanhas");
		modelAndView.addObject("tableTitle", "Tabela de Campanhas cadastradas");
		modelAndView.addObject("tableId", "campaign-table");
		modelAndView.addObject("js", "campaign-list.js");
		modelAndView.addObject("menuId", "campaignlist");
		return modelAndView;
	}

	@GetMapping(value = "/campaignregister")
	public ModelAndView campaignRegister(HttpSession session, Long id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView(path + request.getServletPath());
		Campaign campaign = new Campaign();

		String email = "";
		String phone = "";
		String defaultsmtp = "";
		String hide = "hide";

		if (id != null) {
			Optional<Campaign> campaignOp = campaignRepository.findByIdAndCompany(id, userService.getCurrentUser().getCompany());
			
			if (campaignOp.isPresent()) {
				campaign = campaignOp.get();
				modelAndView.addObject("formTitle", "Editar Campanha");

				if (campaign.getEmail()) {
					email = "checked";
				}
				if (campaign.getPhone()) {
					phone = "checked";
				}

				if (campaign.getDefaultsmtp()) {
					defaultsmtp = "checked";
				}

				hide = "";
				modelAndView.addObject("formId2", "phase-form");
				modelAndView.addObject("tableId2", "phase-table");

				modelAndView.addObject("formId3", "login-form");
				modelAndView.addObject("tableId3", "login-table");
			}
		} else {
			campaign.setCreatedBy(userService.getCurrentUser());
			campaign.setCompany(userService.getCurrentUser().getCompany());
			modelAndView.addObject("formTitle", "Cadastrar Campanha");
		}
			

		modelAndView.addObject("js", "campaign-register.js");
		modelAndView.addObject("formId", "campaign-form");

		modelAndView.addObject("campaign", campaign);

		modelAndView.addObject("email", email);
		modelAndView.addObject("phone", phone);
		modelAndView.addObject("defaultsmtp", defaultsmtp);
		modelAndView.addObject("hide", hide);
		modelAndView.addObject("menuId", "campaignlist");

		return modelAndView;
	}

	@ResponseBody
	@GetMapping(value = "/getcampaigns")
	public List<Campaign> getCampaigns(HttpSession session) {
		Optional<Company> company = companyRepository.findById(userService.getCurrentUser().getCompany().getId());

		try {
			if (company.isPresent()) {
				return campaignRepository.findByCompanyAndCreatedByAndIsactiveTrue(company.get(), userService.getCurrentUser());
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}
	}

	@ResponseBody
	@GetMapping(value = "/getphases")
	public List<Phase> getPhases(Long campaignId, HttpSession session) {
		return phaseRepository.findByCampaignIdAndCompanyIdAndActive(userService.getCurrentUser().getCompany().getId(),
				campaignId);
	}

	@ResponseBody
	@GetMapping(value = "/admin")
	public String getPhases() {
		return "hello";
	}

	@ResponseBody
	@GetMapping(value = "/getlogins")
	public Object getLogins(Long campaignId, HttpSession session) {
		return loginRepository.findByCampaignAndIsactiveTrue( getCampaign(campaignId, session) );
	}

	public Campaign getCampaign(Long campaignId, HttpSession session) {
		Optional<Campaign> optional = campaignRepository.findByIdAndCompany(campaignId,
				userService.getCurrentUser().getCompany());
		return optional.map(c -> {
			return c;
		}).orElse(null);
	}

	@ResponseBody
	@PostMapping(value = "/campaign")
	public ResponseEntity<?> campaignPost(@Valid Campaign campaign,
			BindingResult bindingResult, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,
			HttpSession session) {

		if (bindingResult.hasErrors()) {
		}

		try {
			return new ResponseEntity<Object>(campaignService.save(campaign), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(e.getMessage());
		}
	}

	@ResponseBody
	@GetMapping(value = "/testemailconfig")
	public String testEmailConfig(EmailConfig emailConfig) {
		try {
			emailConfig.setSubject("Adapta Leading Capture Email Teste!");
			emailConfig.setMessage("Parabéns, as suas configurações de email estão funcionado corretamente.");
			return emailService.send(emailConfig);
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	@ResponseBody
	@PostMapping(value = "/phase")
	public ResponseEntity<?> phasePost(@Valid Phase phase, BindingResult bindingResult, Long campaignId,
			HttpServletRequest request, HttpSession session, final RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
		}

		try {
			phase = phaseService.save(phase, campaignId);
		} catch (Exception e) {
			return ResponseEntity.ok(false);
		}
		return ResponseEntity.ok(getPhases(campaignId, session));
	}

	@ResponseBody
	@PostMapping(value = "/logins")
	public ResponseEntity<?> loginPost(@Valid Login login, BindingResult bindingResult, Long campaignId,
			HttpServletRequest request, HttpSession session, final RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
		}
		try {
			login = loginService.save(login, campaignId);
		} catch (Exception e) {
			return ResponseEntity.ok(false);
		}
		return ResponseEntity.ok(getLogins(campaignId, session));
	}

	@ResponseBody
	@PostMapping(value = "/phaseremove")
	public Boolean phaseRemove(Long id) {
		try {
			phaseRepository.findById(id).ifPresent(p -> phaseService.remove(p));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@ResponseBody
	@PostMapping(value = "/loginremove")
	public Boolean loginRemove(Long id) {
		try {
			loginRepository.findById(id).ifPresent(l -> loginRepository.remove(l.getId()));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@ResponseBody
	@PostMapping(value = "/campaignremove")
	public Boolean campaignRemove(Long id) {

		try {
			campaignService.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@ResponseBody
	@PostMapping(value = "/isuniqueseqno")
	public Boolean isUniqueSqNo(Long campaignId, Integer seqNo, Long phaseId) {

		if (phaseId != null) {
			if (phaseRepository.findById(phaseId).isPresent()
					&& phaseRepository.findById(phaseId).get().getSeqNo() != null) {
				if (phaseRepository.findById(phaseId).get().getSeqNo().equals(seqNo)) {
					return false;
				}
			}
		}
		return phaseService.isUniqueSeqNo(campaignId, seqNo);
	}

	@ResponseBody
	@PostMapping(value = "/uploadleads")
	public ResponseEntity<?> uploadLeads(@RequestParam("file") MultipartFile file, Long phaseId) {
		try {
			return ResponseEntity.ok(leadService.uploadLeads(file, phaseId));
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					"O arquivo não pode ser salvo, confira se o arquivo está no formato correto.", null);
		}
	}
}
