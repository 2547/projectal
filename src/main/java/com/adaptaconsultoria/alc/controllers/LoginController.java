package com.adaptaconsultoria.alc.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	@GetMapping(path = {"/login", "", "/"})
	public ModelAndView login(HttpServletRequest request) {
		
		ModelAndView modelAndView = new ModelAndView(request.getServletPath());
		modelAndView.addObject("page", "ALC - Login");
		modelAndView.addObject("js", "login.js");
		modelAndView.addObject("formId", "login-form");
		return modelAndView;
	}
}
