package com.adaptaconsultoria.alc.controllers;

import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.adaptaconsultoria.alc.models.Action;
import com.adaptaconsultoria.alc.objects.enumeration.DeadlineType;
import com.adaptaconsultoria.alc.repositories.ActionRepository;
import com.adaptaconsultoria.alc.services.ActionService;
import com.adaptaconsultoria.alc.services.UserService;

@Controller
@MultipartConfig
@RequestMapping("action")
public class ActionController {

	@Autowired
	private ActionRepository actionRepository;

	@Autowired
	private ActionService actionService;

	@Autowired
	private UserService userService;

	@GetMapping(value = "/register")
	public ModelAndView actionRegister(HttpSession session, Long id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView(request.getServletPath());
		modelAndView.addObject("page", "Ação");
		modelAndView.addObject("title", "Cadastrar de Ações");
		modelAndView.addObject("subTitle", "Cadastrar de Ações");
		modelAndView.addObject("caption", "Cadastre e atualize Ações");
		modelAndView.addObject("tableTitle", "Tabela de Ações cadastradas");
		modelAndView.addObject("tableId", "action-table");
		modelAndView.addObject("formId", "action-form");
		modelAndView.addObject("js", "action-register.js");
		modelAndView.addObject("menuId", "campaignlist");
		modelAndView.addObject("deadlines", getDeadlineTypes());
		modelAndView.addObject("menuId", "actionregister");
		return modelAndView;
	}

	@ResponseBody
	@PostMapping(value = "/actions")
	public ResponseEntity<?> actionPost(@Valid Action action, BindingResult bindingResult, Long phaseId,
			HttpServletRequest request, HttpSession session, final RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
		}
		try {
			action = actionService.save(action, phaseId);
		} catch (Exception e) {
			return ResponseEntity.ok(false);
		}
		return ResponseEntity.ok(getActions(phaseId, session, null));
	}

	@ResponseBody
	@GetMapping(value = "/getactions")
	public List<Action> getActions(Long phaseId, HttpSession session, final RedirectAttributes redirectAttrs) {
		if (phaseId != null) {
			return actionRepository.findByPhaseIdAndActive(phaseId);
		}
		return actionRepository.findByCompanyAndIsactiveTrueAndPhaseIsNull(userService.getCurrentUser().getCompany());
	}

	@ResponseBody
	@RequestMapping(value = "/actionremove", method = RequestMethod.POST)
	public Boolean actionRemove(Long id) {
		try {
			actionRepository.findById(id).ifPresent(p -> actionService.remove(p.getId()));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public DeadlineType[] getDeadlineTypes() {
		return DeadlineType.values();
	}
}
