package com.adaptaconsultoria.alc.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.alc.models.Lead;
import com.adaptaconsultoria.alc.models.LineChartCapturedUser;
import com.adaptaconsultoria.alc.models.LineChartCapturedUserDate;
import com.adaptaconsultoria.alc.models.PieChartVisitedUsersByPhase;
import com.adaptaconsultoria.alc.repositories.CampaignRepository;
import com.adaptaconsultoria.alc.services.DashboardService;
import com.adaptaconsultoria.alc.services.UserService;

@Controller
public class DashboardController {
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private UserService userService;

	@GetMapping(path = "/dashboard")
	public ModelAndView home(HttpSession session, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView(request.getServletPath());
		modelAndView.addObject("page", "ALC - Dashboard");
		modelAndView.addObject("menuId", "dashboard");
		modelAndView.addObject("tableId", "lead-table");
		modelAndView.addObject("campaigns", campaignRepository.findByCompanyAndIsactiveTrue(userService.getCurrentUser().getCompany()));
//		session.setAttribute("role", new ArrayList<>( userService.getCurrentUser().getRoles()).get(0).getName() );
		modelAndView.addObject("cards", dashboardService.getDashboardsByUserId(userService.getCurrentUser().getId()));
		return modelAndView;
	}

	@ResponseBody
	@PostMapping(path = "/getleads")
	public List<Lead> getLeads(HttpSession session, Long campaignId) {
		try {
			return dashboardService.getLeadsByCampaignId(campaignId);
		} catch (Exception e) {
			return null;
		}
	}
	
	@ResponseBody
	@PostMapping(path = "/getlinechartscapturedusersbycampaignid")
	public List<LineChartCapturedUser> getLineChartsCapturedUsersByCampaignId(HttpSession session, Long campaignId) {
		try {
			return dashboardService.getLineChartsCapturedUsersByCampaignId(campaignId);
		} catch (Exception e) {
			return null;
		}
	}
	
	@ResponseBody
	@PostMapping(path = "/getlinechartscapturedusersbycampaignidanddate")
	public List<LineChartCapturedUserDate> getLineChartsCapturedUsersByCampaignIdAndDate(HttpSession session, Long campaignId, String date) {
		try {
			return dashboardService.getLineChartsCapturedUsersByCampaignIdAndDate(campaignId, date);
		} catch (Exception e) {
			return null;
		}
	}
	
	@ResponseBody
	@PostMapping(path = "/getpiechartbycampaignid")
	public List<PieChartVisitedUsersByPhase> getPieChartByCampaignId(HttpSession session, Long campaignId) {
		try {
			return dashboardService.getPieChartByCampaignId(campaignId);
		} catch (Exception e) {
			return null;
		}
	}
}
