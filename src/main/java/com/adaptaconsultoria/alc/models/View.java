package com.adaptaconsultoria.alc.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.Data;

@Data
@Entity
@Table(name = "alc_view")
public class View implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "alc_view_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	private Date created = new Date();

	@ManyToOne
	@JoinColumn(name = "alc_phase_id")
	private Phase phase;

	@OneToMany(mappedBy = "view")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ViewParam> viewParams = new ArrayList<>();

	private String url;

	private String name;

	private String email;

	private String param;

	@Column(length = 40)
	private String phone;

	@Column(name = "ipaddress", length = 40)
	private String ipAddress;

}
