package com.adaptaconsultoria.alc.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "alc_config")
public class Config implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "alc_config_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	private Date created = new Date();

	@ManyToOne
	@JoinColumn(name = "alc_campaign_id")
	private Campaign campaign;

	@Column(name = "smtp_host")
	private String smtpHost;

	@Column(name = "smtp_port")
	private Integer smtpPort;

	@Column(name = "smtp_user")
	private String smtpUser;

	@Column(name = "smtp_password")
	private String smtpPassword;

	@Column(name = "smtp_sender")
	private String smtpSender;

}
