package com.adaptaconsultoria.alc.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class LineChartCapturedUserDate {
	
	@Id
	@Column(name = "p_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "p_hour")
	private String hour;

	@Column(name = "p_viewed")
	private String viewed;

	@Column(name = "p_captured")
	private String captured;
}
