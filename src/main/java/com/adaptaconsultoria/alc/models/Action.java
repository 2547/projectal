package com.adaptaconsultoria.alc.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.adaptaconsultoria.alc.objects.enumeration.DeadlineType;

@Entity
@Table(name = "alc_action")
public class Action implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "alc_action_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	private Date created = new Date();

	@ManyToOne
	@JoinColumn(name = "alc_phase_id")
	private Phase phase;

	@Column(name = "seqno")
	private Integer seqNo;

	@Column(name = "email_subject")
	private String emailSubject;

	@Column(name = "email_text")
	private String emailText;

	@Column(name = "sms_text")
	private String smsText;

	@Column(name = "whatsapp_text")
	private String whatsappText;
	
	@NotNull
	@Column(name = "deadline")
	private Integer deadline;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "deadline_type")
	private DeadlineType deadlineType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	public Integer getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailText() {
		return emailText;
	}

	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	public String getSmsText() {
		return smsText;
	}

	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}

	public String getWhatsappText() {
		return whatsappText;
	}

	public void setWhatsappText(String whatsappText) {
		this.whatsappText = whatsappText;
	}

	public Integer getDeadline() {
		return deadline;
	}

	public void setDeadline(Integer deadline) {
		this.deadline = deadline;
	}

	public String getDeadlineType() {
		return deadlineType.getDescription();
	}
	
	public DeadlineType getDeadlineTypeT() {
		return deadlineType;
	}

	public void setDeadlineType(DeadlineType deadlineType) {
		this.deadlineType = deadlineType;
	}
}

