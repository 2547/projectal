package com.adaptaconsultoria.alc.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "app_session")
public class AppSession implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "app_session_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "app_id")
	private App app;

	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	@NotNull
	@Column(name = "ip_address", length = 40)
	private String ipAddress;

	@NotNull
	@Column(length = 32)
	private String token;

	@NotNull
	private Date created = new Date();

	@NotNull
	private Date validto;

	@NotNull
	@Column(length = 60)
	private String servicename;

	private String serviceargs;

	@NotNull
	private Boolean isactive = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getValidto() {
		return validto;
	}

	public void setValidto(Date validto) {
		this.validto = validto;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getServiceargs() {
		return serviceargs;
	}

	public void setServiceargs(String serviceargs) {
		this.serviceargs = serviceargs;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppSession other = (AppSession) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AppSession [id=" + id + ", company=" + company + ", app=" + app + ", user=" + user + ", ipAddress="
				+ ipAddress + ", token=" + token + ", created=" + created + ", validto=" + validto + ", servicename="
				+ servicename + ", serviceargs=" + serviceargs + ", isactive=" + isactive + "]";
	}

    @Override
    public AppSession clone() throws CloneNotSupportedException {
        return (AppSession) super.clone();
    }

}
