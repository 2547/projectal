package com.adaptaconsultoria.alc.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Data
public class LineChartCapturedUser {
	
	@Id
	@Column(name = "p_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonFormat (shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Column(name = "p_day")
	private Date day;

	@Column(name = "p_viewed")	
	private String viewed;
	
	@Column(name = "p_captured")
	private String captured;
	
}
