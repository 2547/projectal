package com.adaptaconsultoria.alc.models;

import java.util.List;

import com.adaptaconsultoria.alc.objects.in.DefaultIn;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true)
public class EmailSender extends DefaultIn {
	private Long id;
	private List<String> receivers;
	private List<Parameter> parameters;
}
