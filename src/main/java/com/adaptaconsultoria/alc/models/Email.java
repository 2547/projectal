package com.adaptaconsultoria.alc.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "alc_email")
public class Email {

	@Id
	@Column(name = "alc_email_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "alc_phase_id")
	private Phase phase;

	@Column(name = "alcActionId")
	private Integer alcActionId;

	@Column(name = "email")
	private String email;

	@Column(name = "subject")
	private String subject;

	@Column(name = "message")
	private String message;

	@Column(name = "send")
	private Boolean send;

	private Date sendtime = new Date();

	private Date viewed = new Date();
	
	@Column(name = "failed")
	private Boolean failed;
 }
