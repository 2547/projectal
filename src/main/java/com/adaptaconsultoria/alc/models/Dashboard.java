package com.adaptaconsultoria.alc.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Dashboard {

	@Id
	@Column(name = "p_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "p_indicador")
	private String indicador;

	@Column(name = "p_valor")
	private String valor;
	
	@Column(name = "p_color")	
	private String color;
	
	@Column(name = "p_icon")
	private String icon;
}
