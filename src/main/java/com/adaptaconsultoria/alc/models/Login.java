package com.adaptaconsultoria.alc.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "alc_login")
public class Login implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "alc_login_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "alc_campaign_id")
	private Campaign campaign;

	@NotNull
	private String name;

	@NotNull
	private String login;

	private String email;
	
	private String cellphone;
}
