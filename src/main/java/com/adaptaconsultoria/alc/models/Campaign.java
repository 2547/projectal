package com.adaptaconsultoria.alc.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "alc_campaign")
@Data
public class Campaign implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "alc_campaign_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private String name;

	@Column(length = 500)
	private String description;

	@NotNull
	private Boolean email;

	@NotNull
	private Boolean phone;

	@NotNull
	@Column(length = 40)
	private String token;

	private Integer qtyLogins;
	private String registerUrl;
	private String smtpHost;
	private String smtpPort;
	private String smtpUser;
	private String smtpPassword;
	private String smtpSender;
	
	private Boolean defaultsmtp;
}
