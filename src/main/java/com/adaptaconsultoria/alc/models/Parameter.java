package com.adaptaconsultoria.alc.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true)
public class Parameter {
	private String name;
	private String value;
}
