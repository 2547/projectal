package com.adaptaconsultoria.alc.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "company_role")
public class CompanyRole implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "company_role_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	@Column
	private String theme;

	@Column(name = "menu_layout")
	private String menuLayout;

	@Column
	private Boolean isdefault;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUsers() {
		return user;
	}

	public void setUsers(User user) {
		this.user = user;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getMenuLayout() {
		return menuLayout;
	}

	public void setMenuLayout(String menuLayout) {
		this.menuLayout = menuLayout;
	}

	public Boolean getIsdefault() {
		return isdefault;
	}

	public void setIsdefault(Boolean isdefault) {
		this.isdefault = isdefault;
	}
}