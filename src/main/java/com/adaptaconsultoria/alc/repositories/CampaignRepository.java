package com.adaptaconsultoria.alc.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.User;

public interface CampaignRepository extends JpaRepository<Campaign, Long> {
	
	public Optional<Campaign> findByIdAndIsactiveTrue(Long id);

	public Optional<Campaign> findByIdAndCompany(Long id, Company company);
	
	public List<Campaign> findByCompanyAndIsactiveTrue(Company company);

	public List<Campaign> findByCompanyAndCreatedByAndIsactiveTrue(Company company, User user);

}
