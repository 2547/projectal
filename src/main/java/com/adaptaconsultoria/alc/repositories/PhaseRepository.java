package com.adaptaconsultoria.alc.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Phase;

public interface PhaseRepository extends JpaRepository<Phase, Long> {

	@Query("select p from Phase p where replace(replace(p.url, 'http://', ''), 'https://', '') = :url and campaign = :campaign")
	public Phase findTop1ByUrlAndCampaign(@Param("url") String url, @Param("campaign") Campaign campaign); 
	
	@Query("SELECT obj FROM Phase obj WHERE obj.company.id = :companyId AND obj.campaign.id = :campaignId and obj.isactive = true")
	public List<Phase> findByCampaignIdAndCompanyIdAndActive(@Param("companyId") Long companyId, @Param("campaignId") Long campaignId);
	
	@Query("SELECT obj FROM Phase obj WHERE obj.company.id = :companyId AND obj.seqNo = :seqNo and obj.campaign.id = :campaignId")
	public Optional<Phase> findByCompanyAndSeqNoAndCampaign(
				@Param("companyId") Long companyId, 
				@Param("seqNo") Integer seqNo, 
				@Param("campaignId") Long campaignId );
		
	@Transactional
	@Modifying
	@Query("DELETE FROM Phase obj where obj.id = :id")
	public void remove(@Param("id") Long id);
	
	@Query("SELECT obj FROM Phase obj WHERE obj.campaign.id = :id AND obj.seqNo = :seqNo and obj.isactive = true")
	public Optional<Phase> findByCampaignIdAndSeqNo( @Param("id") Long campaignId, @Param("seqNo") Integer seqNo );
	
	public Optional<Phase> findByIdAndIsactiveTrue(Long id);
	
	public List<Phase> findByCampaignAndIsactiveTrue(Campaign campaign);
	
	public Optional<List<Optional<Phase>>> findOpByCampaignAndIsactiveTrue(Campaign campaign);
}
