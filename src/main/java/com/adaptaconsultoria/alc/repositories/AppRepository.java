package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.App;

public interface AppRepository extends JpaRepository<App, Long> {

	App findTop1ByTokenAndIsactiveTrue(String token);

	App findTop1ByTokenAndPasswordAndIsactiveTrue(String appToken, String appPassword);

}
