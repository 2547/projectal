package com.adaptaconsultoria.alc.repositories;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adaptaconsultoria.alc.models.Campaign;
import com.adaptaconsultoria.alc.models.Login;

public interface LoginRepository extends JpaRepository<Login, Long> {
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Login obj where obj.id = :id")
	public void remove(@Param("id") Long id);
	
	public Set<Login> findByCampaignAndIsactiveTrue(Campaign campaign);
	
	public Optional<List<Optional<Login>>> findOpByCampaignAndIsactiveTrue(Campaign campaign);
}
