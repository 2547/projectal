package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adaptaconsultoria.alc.models.Email;

public interface EmailRepository extends JpaRepository<Email, Long> {
	
	@Query(value = "select * from generate_queue_email()", nativeQuery = true)
	public Integer generateQueueEmail();
	
	public List<Email> findBySendFalse();
}
