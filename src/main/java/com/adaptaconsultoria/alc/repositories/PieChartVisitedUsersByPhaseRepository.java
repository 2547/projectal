package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.PieChartVisitedUsersByPhase;

public interface PieChartVisitedUsersByPhaseRepository extends JpaRepository<PieChartVisitedUsersByPhase, Long> {
	
	@Query(value = " select * from alc_dashboard_phase(:campaignId) ", nativeQuery = true)
	public List<PieChartVisitedUsersByPhase> getPieChartByCampaignId(@Param("campaignId") Long campaignId);
	
}
