package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.IPBlocked;

public interface IPBlockedRepository extends JpaRepository<IPBlocked, Long> {

	IPBlocked findTop1ByIpAddressAndIsactiveTrue(String ipAddress);

}
