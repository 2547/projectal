package com.adaptaconsultoria.alc.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.AppSession;

public interface AppSessionRepository extends JpaRepository<AppSession, Long> {

	AppSession findTop1ByTokenAndIpAddressAndValidtoGreaterThanAndIsactiveTrue(String token, String ipAddress, Date validTo);

	AppSession findTop1ByTokenAndIsactiveTrue(String token);

}
