package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.Lead;

public interface LeadRepository extends JpaRepository<Lead, Long> {

	@Query(value = " select * from alc_dashboard_leads(:companyId) ", nativeQuery = true)
	public List<Lead> getLeadByCompanyId (@Param("companyId") Long companyId);
	
}
