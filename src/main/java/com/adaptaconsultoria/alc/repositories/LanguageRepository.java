package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.Language;

public interface LanguageRepository extends JpaRepository<Language, Long> {

	List<Language> findByIsactiveTrueOrderByName();

}
