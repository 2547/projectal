package com.adaptaconsultoria.alc.repositories;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.LineChartCapturedUserDate;

public interface LineChartCapturedUserDateRepository extends JpaRepository<LineChartCapturedUserDate, Long> {
	
	@Query(value = " select * from alc_dashboard_datetime(:companyId, :date) ", nativeQuery = true)
	public List<LineChartCapturedUserDate> getLineChartsCapturedUsersByCampaignIdAndDate(@Param("companyId") Long companyId, @Param("date") Date date);
	
}
