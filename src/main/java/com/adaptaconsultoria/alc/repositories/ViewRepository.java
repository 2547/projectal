package com.adaptaconsultoria.alc.repositories

;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adaptaconsultoria.alc.models.View;

public interface ViewRepository extends JpaRepository<View, Long> {
	public View findFirstByEmail(String email);

	@Query("SELECT obj FROM View obj")
	public View findAllByUrl();
}
