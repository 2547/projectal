package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adaptaconsultoria.alc.models.Action;
import com.adaptaconsultoria.alc.models.Company;

public interface ActionRepository extends JpaRepository<Action, Long> {

	@Query("SELECT obj FROM Action obj WHERE obj.phase.id = :phaseId AND obj.isactive = true")
	public List<Action> findByPhaseIdAndActive(@Param("phaseId") Long phaseId);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Action obj where obj.id = :id")
	public void remove(@Param("id") Long id);
	
	public List<Action> findByCompanyAndIsactiveTrueAndPhaseIsNull(Company company);
}
