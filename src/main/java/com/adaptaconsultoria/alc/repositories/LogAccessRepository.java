package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.LogAccess;

public interface LogAccessRepository extends JpaRepository<LogAccess, Long> {

}