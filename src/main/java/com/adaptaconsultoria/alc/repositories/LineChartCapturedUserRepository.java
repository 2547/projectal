package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.LineChartCapturedUser;

public interface LineChartCapturedUserRepository extends JpaRepository<LineChartCapturedUser, Long> {

	@Query(value = " select * from alc_dashboard_date(:companyId) ", nativeQuery = true)
	public List<LineChartCapturedUser> getLineChartsCapturedUsersByCampaignId (@Param("companyId") Long companyId);
	
}
