package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.ViewParam;

public interface ViewParamRepository extends JpaRepository<ViewParam, Long> {

}
