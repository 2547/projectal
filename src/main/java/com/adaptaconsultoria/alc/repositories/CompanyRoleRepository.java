package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.CompanyRole;
import com.adaptaconsultoria.alc.models.Role;
import com.adaptaconsultoria.alc.models.User;

public interface CompanyRoleRepository extends JpaRepository<CompanyRole, Long> {
	CompanyRole findTop1ByCompanyAndRoleAndUser(Company company, Role role, User user);
	List<CompanyRole> findByUser(User user);
}
