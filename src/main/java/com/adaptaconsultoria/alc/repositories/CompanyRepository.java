package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> 
{

}
