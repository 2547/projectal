package com.adaptaconsultoria.alc.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.Config;

public interface ConfigRepository extends JpaRepository<Config, Long> {
	List<Config> findByCompanyId(@Param("companyId") String companyId);
	List<Config> findByCompany(Company company);
	Optional<Config> findTop1ByCompanyAndIsactiveTrue(Company company);
}
