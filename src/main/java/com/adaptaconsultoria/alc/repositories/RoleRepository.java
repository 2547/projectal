package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}