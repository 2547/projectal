package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.Company;
import com.adaptaconsultoria.alc.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u where lower(u.login) = lower(:login) order by u.login")
	User findTop1ByLoginOrderByLogin(@Param("login") String login);

	@Query("select u from User u where lower(u.email) = lower(:email) order by u.email")
	User findTop1ByEmailOrderByEmail(@Param("email") String email);

	User findTop1ByLoginAndPasswordAndStatusTrueOrderByLogin(String login, String password);

	List<User> findByCompanyOrderByName(Company company);
	
	User findByLogin(String login);
	
	User findTop1ByCompanyAndLoginAndStatusTrueOrderByLogin(Company company, String login);
}
