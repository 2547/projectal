package com.adaptaconsultoria.alc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.alc.models.Dashboard;

public interface DashboardRepository extends JpaRepository<Dashboard, Long> {
	
	@Query(value = " select * from alc_dashboard(:userId) ", nativeQuery = true)
	public List<Dashboard> getDashboardByUserId(@Param("userId") Long userId);
	
}
