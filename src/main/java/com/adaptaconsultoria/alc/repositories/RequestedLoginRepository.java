package com.adaptaconsultoria.alc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.alc.models.RequestedLogin;

public interface RequestedLoginRepository extends JpaRepository<RequestedLogin, Long> {
	
}
