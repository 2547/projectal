<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>

<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
<!-- Include Editor style. -->
</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<!--breadcrumbs start-->
				<div id="breadcrumbs-wrapper">
					<!-- Search for small screen -->
					<div class="header-search-wrapper grey lighten-2 hide-on-large-only">
						<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
					</div>
					<div class="container">
						<div class="row">
							<div class="col s10 m6 l6">
								<h5 class="breadcrumbs-title">${title}</h5>
								<ol class="breadcrumbs">
									<li><a href="${pageContext.request.contextPath}/config/list">Lista de Configurações</a></li>
								</ol>
							</div>
						</div>
						<div id="basic-form" class="section">
							<div class="col s12 m12 l6">
								<div class="card-panel">
									<div class="row">
										<div class="row">
											<form:form class="col s12" id="${formId}"  modelAttribute="${modelAttribute}">
												<h4 class="header2">${title}</h4>
												<form:input type="hidden" path="id" name="id" id="id"/>
												<div class="row">
													<div class="input-field col s12">
														<form:input path="smtpHost" id="smtpHost" type="text" name="smtpHost" maxlength="100" class="required"/> 
														<label for="smtpHost">Host*</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<form:input path="smtpPort" id="smtpPort" class="required" type="text" name="smtpPort" maxlength="100"/> <label for="smtpPort">Porta*</label>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="input-field col s12">
														<form:input path="smtpUser" id="smtpUser" class="required" type="text" name="smtpUser" maxlength="100" /> <label for="smtpUser">SMTP Usuário*</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<form:input path="smtpPassword" class="required" id="smtpPassword" type="text" name="smtpPassword" maxlength="100" /> <label for="smtpPassword">SMTP Senha*</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<form:input path="smtpSender" class="required" id="smtpSender" type="text" maxlength="100" name="smtpSender" /> <label for="smtpSender">SMTP Remetente*</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<button class="btn cyan waves-effect waves-light right gradient-45deg-light-blue-cyan" type="submit" name="action">
															<i class="material-icons right">send</i>Enviar
														</button>
														<a href="${pageContext.request.contextPath}/config/list" class="waves-effect waves-light btn gradient-45deg-red-pink box-shadow-none mr-1 mb-1" type="submit" name="action" >
															Voltar<i class="material-icons right">reply</i>
														</a>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!-- END MAIN -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>
</body>
</html>

