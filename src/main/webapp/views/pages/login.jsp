<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================ -->

<head>
<jsp:include page="../tiles/templates/css.jsp"></jsp:include>
<link href="${pageContext.request.contextPath}/resources/css/layouts/page-center.css" type="text/css" rel="stylesheet">

<!-- Favicons-->
<link rel="icon" href="${pageContext.request.contextPath}/resources/images/favicon/favicon-32x32.png" sizes="32x32">
<!-- Favicons-->
<link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/resources/images/favicon/apple-touch-icon-152x152.png">
<!-- For iPhone -->
<meta name="msapplication-TileColor" content="#00bcd4">
<meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/images/favicon/mstile-144x144.png">
<!-- For Windows Phone -->
</head>

<body class="cyan">
	<!-- Start Page Loading -->
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	</div>
	<!-- End Page Loading -->
	<div id="login-page" class="row">
		<div class="col s12 z-depth-4 card-panel">
			<form class="login-form" action="login" method="post" id="${formId}">

				<div class="row">
					<div class="input-field col s12 center">
						<img src="${pageContext.request.contextPath}/resources/images/logo/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
						<p class="center login-form-text">ALC ${message}</p>
					</div>
				</div>
				<c:if test="${param.error ne null}">
					<div class="row ">
						<div class="input-field col s12">
							<div id="card-alert" class="card gradient-45deg-purple-deep-orange">
								<div class="card-content white-text">
									<p class="center-align">Login ou Senha inválido!</p>
								</div>
								<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
						</div>
					</div>
				</c:if>

				<div class="row margin">
					<div class="input-field col s12">
						<i class="material-icons prefix pt-5">person_outline</i> <input id="login" type="text" name="login" required>
						<div class="input-field col s2"></div>
						<label for="login" class="center-align">Login</label>
					</div>
				</div>
				<div class="row margin">
					<div class="input-field col s12">
						<i class="material-icons prefix pt-5">lock_outline</i> <input id="password" type="password" name="password" required>
						<div class="input-field col s2"></div>
						<label for="password">Senha</label>
					</div>
				</div>
				<div class="row hide">
					<div class="col s12 m12 l12 ml-2 mt-3">
						<input type="checkbox" id="remember-me" /> <label for="remember-me">Remember me</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<button type="submit" class="btn waves-effect waves-light col s12">Login</button>
					</div>
				</div>
				<div class="row hide">
					<div class="input-field col s6 m6 l6">
						<p class="margin medium-small">
							<a href="page-register.html">Register Now!</a>
						</p>
					</div>
					<div class="input-field col s6 m6 l6">
						<p class="margin right-align medium-small">
							<a href="page-forgot-password.html">Forgot password ?</a>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<jsp:include page="../tiles/templates/js.jsp"></jsp:include>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/login.js"></script>
<c:if test="${message ne null}">
	<script>
		Materialize.toast('Login ou senha invalida!', 4000, 'red')
	</script>
</c:if>
</html>