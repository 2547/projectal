<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../tiles/templates/header.jsp"></jsp:include>

<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<link href="${pageContext.request.contextPath}/resources/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/vendors/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<!--start container-->
				<div class="container">
					<!--card stats start-->
					<div id="card-stats">
						<div class="row">
							<c:forEach items="${cards}" var="card">
								<div class="col s12 m6 l3">
									<div class="card ${card.color} gradient-shadow min-height-100 white-text">
										<div class="padding-4">
											<div class="col s9 m9">
												<i class="material-icons background-round mt-5">${card.icon}</i>
												<p>${card.indicador}</p>
											</div>
											<div class="col s3 m3 right-align">
												<h5 class="mb-0">${card.valor}</h5>
												<p class="no-margin">Total</p>
												<p></p>
											</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
					<!--card stats end-->
					<!--yearly & weekly revenue chart start-->
					<div class="col s2 m6 l6 push-s5">
						<a class="btn dropdown-settings waves-effect waves-light breadcrumbs-btn gradient-45deg-light-blue-cyan gradient-shadow" href="#!" data-activates="dropdown1"> <i
							class="material-icons hide-on-med-and-up">settings</i> <span class="hide-on-small-onl">Selecione uma Campanha</span> <i class="material-icons right">arrow_drop_down</i>
						</a>
						<ul id="dropdown1" class="dropdown-content">
							<c:forEach items="${campaigns}" var="campaign">
								<li><a href="#!" class="grey-text text-darken-2 cselected" id="${campaign.id}"> ${campaign.name} </a></li>
							</c:forEach>
						</ul>
					</div>

					<div class="row">
						<div class="col s12 m6 l6">
							<div id="revenue-chart" class="card">
								<div class="card-content">
									<h4 class="header mt-0">
										<span id="title-chart"> LEADS Por campanhas </span> <a class="waves-effect waves-light btn gradient-45deg-purple-deep-orange gradient-shadow right disabled" id="reset-chart">Reiniciar</a>
									</h4>
									<div class="row">
										<div class="col s12">
											<div class="yearly-revenue-chart" id="chart-div">
												<div id="card-alert" class="card cyan center-align non-selected-campaign-alert">
													<div class="card-content white-text">
														<p>Selecione uma campanha para obter os an�lise relacionados.</p>
													</div>
												</div>
												<canvas id="viewed" class="firstShadow" height="350"></canvas>
												<canvas id="captured" height="350"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="revenue-chart" class="card">
								<div class="card-content">
									<h4 class="header mt-0">Acessos por Fase</h4>
									<div class="row">
										<div class="col s12">
											<div id="card-alert" class="card cyan center-align non-selected-campaign-alert">
												<div class="card-content white-text">
													<p>Selecione uma campanha para obter os an�lise relacionados.</p>
												</div>
											</div>
											<div class="yearly-revenue-chart">
												<canvas id="pie-chart" height="350"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m6 m6">
							<div id="weekly-earning" class="card">
								<div class="card-content">
									<h4 class="header mt-0">Leads Capturados</h4>
									<table id="${tableId}" class="display" style="width: 100%">
										<thead>
											<tr>
												<th>Id</th>
												<th>Nome</th>
												<th>Email</th>
												<th>Telefone</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->

			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../tiles/templates/js.jsp"></jsp:include>

	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/scripts/dashboard-analytics.js"></script> --%>
	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/scripts/dashboard-ecommerce.js"></script> --%>
	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/scripts/google-map-script.js"></script> --%>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/dashboard.js"></script>
</body>
</html>