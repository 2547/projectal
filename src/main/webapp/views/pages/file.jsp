<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? 
	include "header.php";
?>

<script language="javascript" type="text/javascript">
function validaEmail (email) {
	return email.match( /^(([^\W]([\w_\.\-])*[^\W])|(\w))@(([\w_\-])+\.)+([a-zA-Z0-9]{2,4})+$/ ) ? true : false;
}

function validar(){
	if (document.formulario.nomeremetente.value == ""){
		alert("Informe seu nome completo.");
		document.formulario.nomeremetente.focus();
		return false;
	}

	if (document.formulario.emailremetente.value == ""){
		alert("Informe seu e-mail.");
		document.formulario.emailremetente.focus();
		return false;
	}
	else{
		if (validaEmail(document.formulario.emailremetente.value) == false){
		alert("O e-mail parece estar incorreto.");
		document.formulario.emailremetente.focus();
		return false;
		}
	}
	if (document.formulario.assunto.value == ""){
		alert("Informe o assunto.");
		document.formulario.assunto.focus();
		return false;
	}	
	if (document.formulario.mensagem.value == ""){
		alert("Digite sua mensagem.");
		document.formulario.mensagem.focus();
		return false;
	}
	return true;
}
</script>

</head>
<body>
<div class="site">
    <?
    include "menu.php";
	?>
    <div class="clearfix"></div>
    <div>

		<ul class="breadcrumb clearfix">
			<li><a href="index.php">Home</a> <span class="divider">&raquo;</span></li>
			<li class="active">Contato</li>
		</ul>

    </div>


  <div class="row-fluid conteudo">
  	<div class="span8 margin20">
       	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3551.5197748605174!2d-52.613685584950936!3d-27.108433983043653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e4b6a17d42b265%3A0x46d89bc913ff93a7!2sRenova+Marcas+%26+Patentes!5e0!3m2!1spt-BR!2sbr!4v1548095007746" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3551.5197748605174!2d-52.613685584950936!3d-27.108433983043653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e4b6a17d42b265%3A0x46d89bc913ff93a7!2sRenova+Marcas+%26+Patentes!5e0!3m2!1spt-BR!2sbr!4v1548095007746" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
       	  
       	  Exibir mapa ampliado</a></small>
    <address class="margin20">
        <h4>Endere&ccedil;o:</h4>
        Av. Nereu Ramos, 454D - Centro, Chapecó - SC, 89801-023 | +55 49 3324-3500
    </address>
    </div>
  	<div class="span4 margin20">

	<?

	if (isset($_POST[nomeremetente])){

		if (eregi('tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$', $_SERVER[HTTP_HOST])) {
			$emailsender='contato@adaptaconsultoria.com.br';
		}
		else {
			$emailsender = "contato@adaptaconsultoria.com.br";
		}		
		/* Verifica qual � o sistema operacional do servidor para ajustar o cabe�alho de forma correta. */
		if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
		elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows
		else die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

		$nomeremetente     = $_POST['nomeremetente'];
		$emailremetente    = trim($_POST['emailremetente']);
		$assunt            = $_POST['assunto'];
		$mensagem          = $_POST['mensagem'];

		$emaildestinatario = "contato@adaptaconsultoria.com.br";
		$comcopia          = trim($_POST['comcopia']);
		$assunto           = "Contato do site - Adapta ".$assunt;

		$mensagemHTML = '<font face="Verdana, Arial" color="#555555" size="2">';
		$mensagemHTML .= 'Remetente: '.$nomeremetente.'<br> ';
		$mensagemHTML .= 'E-mail: '.$emailremetente.'<br> ';
		$mensagemHTML .= 'Assunto: '.$assunt.'<br> ';
		$mensagemHTML .= 'Mensagem:<br><b>'.$mensagem.'</b><br><hr>';
		$mensagemHTML .= '</font>';

		$headers = "MIME-Version: 1.1".$quebra_linha;
		$headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
		$headers .= "From: Site Adapta <".$emailsender.">".$quebra_linha;
		$headers .= "Return-Path: " . $emailsender . $quebra_linha;
		if(strlen($comcopia) > 0) $headers .= "Cc: ".$comcopia.$quebra_linha;
		$headers .= "Reply-To: ".$emailremetente.$quebra_linha;

		@mail($emaildestinatario, $assunto, $mensagemHTML, $headers, "-r". $emailsender);
		
		echo "<div class=\"fonte_aaux\" style=\"padding:10px; color:#333333; font-size:14px;\">
		Mensagem enviada com sucesso.<br><br>
		Responderemos seu e-mail o mais breve possível.
		<br>
		</div>";



	}
	else{
	?>
    <form class="contato" method="post" action="contato.php" name="formulario" onsubmit="return validar();">
    <fieldset>
        <legend class="fonte_aaux">FORMULÁRIO DE CONTATO</legend>
        <label>Nome</label>
        <input type="text" class="span12" placeholder="Digite seu nome" name="nomeremetente">
        <label>E-mail</label>
        <input type="text" class="span12" placeholder="Digite seu e-mail" name="emailremetente">
        <label>Assunto</label>
        <input type="text" class="span12" placeholder="Informe o assunto" name="assunto">
        <label>Mensagem</label>
		<textarea name="mensagem" rows="3"></textarea>
		<div class="text-right">
		<button type="submit"><img src="img/enviar_bt.png" /></button>
        </div>
    </fieldset>
    </form>        
	<?
	}
	?>

    </div>
  </div>

  <?
  	include "rodape.php";
  ?>
  
</div>
</body>
</html>
