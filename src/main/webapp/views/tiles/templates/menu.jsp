<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible">
	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a href="index.html" class="brand-logo darken-1"> <img src="${pageContext.request.contextPath}/resources/images/logo/materialize-logo.png" alt="materialize logo"> <span
				class="logo-text hide-on-med-and-down">Materialize</span>
			</a> <a href="#" class="navbar-toggler"> <i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>
	<ul id="slide-out" class="side-nav fixed leftside-navigation">
		<li class="no-padding">
			<ul class="collapsible" data-collapsible="accordion">
				<li class="bold" id="dashboard"><a href="${pageContext.request.contextPath}/dashboard" class="waves-effect waves-cyan"> <i class="material-icons">dashboard</i> <span class="nav-text">Home</span>
				</a></li>
				<li class="bold" id="campaignlist"><a href="${pageContext.request.contextPath}/campaignlist" class="waves-effect waves-cyan"> <i class="material-icons">today</i> <span class="nav-text">Cadastrar Campanha</span>
				</a></li>
				<li class="bold" id="actionregister"><a href="${pageContext.request.contextPath}/action/register" class="waves-effect waves-cyan"> <i class="material-icons">input</i> <span class="nav-text">Cadastrar A��o</span>
				</a></li>
				<li class="bold" id="config"><a href="${pageContext.request.contextPath}/config/list" class="waves-effect waves-cyan"> <i class="material-icons">settings</i> <span class="nav-text">Configura��o</span>
				</a></li>
			</ul>
		</li>
	</ul>
	<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow"> <i
		class="material-icons">menu</i>
	</a>
</aside>
<script> 
	var id = "${menuId}"; 
	var d = document.getElementById(id);
	d.className += ' active';
</script>
<!-- END LEFT SIDEBAR NAV-->
