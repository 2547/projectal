<jsp:include page="meta.jsp"></jsp:include>

<!-- CORE CSS-->
<link href="${pageContext.request.contextPath}/resources/css/themes/collapsible-menu/materialize.css" type="text/css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/themes/collapsible-menu/style.css" type="text/css" rel="stylesheet">
<!-- Custome CSS-->
<link href="${pageContext.request.contextPath}/resources/css/custom/custom.css" type="text/css" rel="stylesheet">
<!-- Included Plugin CSS ON THIS PAGE -->
<link href="${pageContext.request.contextPath}/resources/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">

<!-- Data Tables-->
<link href="${pageContext.request.contextPath}/resources/vendors/data-tables/css/jquery.dataTables.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="${pageContext.request.contextPath}/resources/vendors/data-tables/css/select.dataTables.css" type="text/css" rel="stylesheet" media="screen,projection">

<!-- sweetalert css-->
<link href="${pageContext.request.contextPath}/resources/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet" media="screen,projection">

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.dataTables.css"/> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

