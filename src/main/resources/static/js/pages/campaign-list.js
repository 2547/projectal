var table;

$(document).ready(function() {
	openTable()
})

function openTable() {

	$.ajax({
		url : "getcampaigns",
		beforeSend : function() {
			if ($("#progress").hasClass("hide")) {
				$("#progress").removeClass("hide");
				$("#campaign-table").addClass("hide");
			} else {
				$("#progress").addClass("hide");
			}
		},
		success : function(obj) {
			table(obj)
		}
	})
}

function table(obj) {
	console.log(obj)
	table = $("#campaign-table")
			.DataTable(
					{
						data : obj,
						language : {
							"url" : "/alc/resources/vendors/data-tables/json/Portuguese-Brasil.json"
						},
						columns : [ {
							data : "id"
						}, {
							data : null, "defaultContent": '', 'title': ''
						}, {
							data : "name"
						}, {
							data : "description"
						}, {
							data : "token"
						}, {
							data : "email"
						}, {
							data : "phone"
						}, {
							data : "qtyLogins"
						}, {
							data : "registerUrl"
						}, {
							data : "smtpHost"
						}, {
							data : "smtpPort"
						}, {
							data : "smtpUser"
						}, {
							data : "smtpPassword"
						}, {
							data : "smtpSender"
						}, {
							data : "companyId",
							"mRender" : function(data, type, full) {
								return full.id;
							}
						} ],
						columnDefs : [ {
							orderable : false,
							className : 'select-checkbox',
							targets : 1
						}, {
							orderable : false,
							width : 100,
							targets : 6
						}, {
							"targets" : [ 0, 8 , 3, 5, 6, 7, 14 ],
							"visible" : false,
						}, {
							className : "dt-center",
							"targets" : [ 10 ]
						}],
						dom : 'Bfrtip',
						select : true,
					    buttons: [
				            {
				                text: '<i class="material-icons left">add</i></a>',
				                className : 'btn-floating waves-effect waves-light purple lightrn-1',
				                action: function ( e, dt, node, config ) {
				                	window.location.href = "campaignregister";
				                },
				                enabled: true
				            },
				            {
				                text: '<i class="material-icons left">edit</i></a>',
				                className : 'btn-floating waves-effect',
				                action: function ( e, dt, node, config ) {
				                	var campaign = dt.row( { selected: true } ).data();
				                	window.location.href = "campaignregister?id="+campaign.id;
				                },
				                enabled: false
				            },
				            {
				                text: '<i class="material-icons left ">clear</i></a>',
				                className : 'btn-floating waves-effect waves-light red',
				                action: function ( e, dt, node, config ) {
				                	var campaign = dt.row( { selected: true } ).data();
				                	campaignRemove(campaign.id);
				                },
				                enabled: false
				            }
				        ],
						select : {
							style : 'os',
							selector : 'td:first-child'
						},
						initComplete: function(settings, json) {
							selectTableConfig(this.DataTable());
					    },
						order : [ [ 1, 'asc' ] ]
					});
}

function campaignRemove(id) {
	swal(
			{
				title : "Você tem certeza que deseja cancelar?",
				text : "Não será possível recuperar esse registro após o cancelamento!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : '#DD6B55',
				confirmButtonText : 'Sim!',
				cancelButtonText : "Não!",
				closeOnConfirm : false,
				closeOnCancel : false
			}, function(isConfirm) {
				if (isConfirm) {
					$.ajax({
						type : "POST",
						data : {
							"id" : id
						},
						url : "campaignremove",
						success : function(obj) {
							if (obj) {
								table.row({
									selected : true
								}).remove();
								table.draw();
								clearCampaignFields()
								selectDeselectTable(table)
								swal("Removido!", "O registro foi removido!",
										"success");
							} else {
								swal("Cancelado",
										"O registro não pode ser removido!",
										"error");
							}
						}
					})
				} else
					swal("Cancelado", "O registro não foi removido!", "error");
			});
}

function clearCampaignFields() {
	table.rows('.selected').deselect();
}

function selectTableConfig(table) {
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	
	$('#' +table.tables().nodes().to$().attr('id')+ ' tbody').on('dblclick', 'tr', function () {
		table.rows('.selected').deselect();
		table.rows(this).select()
	} );
	
	$('#' +table.tables().nodes().to$().attr('id')+ ' tbody').on('click', 'tr', function () {
		console.log('hi')
	} );
}

function selectDeselectTable(table) {
    var selectedRows = table.rows( { selected: true } ).count();
    table.button( 1 ).enable( selectedRows > 0 );
    table.button( 2 ).enable( selectedRows > 0 );
}












