var descTempt;
var table;
var phaseId;

$(document).ready(function() {
	validator();
	tiny();
	phaseId = getParamUrl().phaseId;
	openTable();
	mask();
	initializeEditor();
	initializeModal();
});

function redirect() {
	if (getParamUrl().campaignId)
		window.location.href = contextPath + "campaignregister?id=" + getParamUrl().campaignId;
	else 
		window.location.href = contextPath + "campaignregister";
}

function initializeModal() {
	$('.modal').modal();
}

function removeText() {
	return tinyMCE.activeEditor.setContent('');
}

function getText() {
	return tinyMCE.activeEditor.getContent({format : 'raw'});
}

function insertHtml(html) {
	return tinymce.activeEditor.setContent(html, {format: 'raw'});
}

function initializeEditor() {
	tinymce.init({ 
		selector:'#emailText-action' 
	});
}

function mask() {
	$('.phone').mask('(00) 00000-0000');
}

function openTable(id) {
    if (table != undefined && table != '')
        table.destroy();

    $.ajax({
        url: "getactions",
        data: {
            "phaseId": phaseId
        },
        success: function(obj) {
            console.log(obj)
            table = $('#action-table').DataTable({
                data: obj,
                language: {
                    "url": "/alc/resources/vendors/data-tables/json/Portuguese-Brasil.json"
                },
                buttons: [{
                        text: '<i class="material-icons left">arrow_back</i></a>',
                        className: 'btn-floating waves-effect waves-light cyan tooltipped',
                        attr: {
                            'data-position': 'top',
                            'data-delay': '50',
                            'data-tooltip': 'Voltar'
                        },
                        action: function(e, dt, node, config) {
                            window.location.href = contextPath + "campaignregister?id=" + getParamUrl().campaignId;
                        },
                        enabled: true
                    }, {
                        text: '<i class="material-icons left">clear_all</i></a>',
                        className: 'btn-floating waves-effect waves-light purple lightrn-1 tooltipped',
                        attr: {
                            'data-position': 'top',
                            'data-delay': '50',
                            'data-tooltip': 'Limpar Formulário'
                        },
                        action: function(e, dt, node, config) {
                            clearActionFields()
                        },
                        enabled: false
                    }, {
                        text: '<i class="material-icons left ">clear</i></a>',
                        className: 'btn-floating waves-effect waves-light red tooltipped',
                        attr: {
                            'data-position': 'top',
                            'data-delay': '50',
                            'data-tooltip': 'Remover Registro'
                        },
                        action: function(e, dt, node, config) {
                            var obj = dt.row({
                                selected: true
                            }).data();
                            actionRemove(obj.id)
                        },
                        enabled: false
                    }, {
                        text: '<i class="material-icons right">send</i>',
                        className: 'btn waves-effect waves-light cyan right',
                        action: function(e, dt, node, config) {
                            var obj = dt.row({
                                selected: true
                            }).data();
                            save()
                        },
                        enabled: true
                    },
                ],
                select: 'single',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                columns: [{
                        data: "id"
                    }, {
                        data: null,
                        "defaultContent": '',
                        'title': ''
                    }, {
                        data: "phase",
                        mRender: function(data, type, full) {
                        	if (full.phase != null) 
                        		return full.phase.id;
                        	return '';
                        }
                    },
                    {
                        data: "seqNo",
                        "title": "Nro Seq",
                        "width": "5px"
                    }, {
                        data: null,
                        title: "Email",
                        defaultContent: "<a id=\"email-action\" class=\"btn-flat waves-effect\"> " +
                            "	<i class=\"fas fa-envelope\"></i> " +
                            "Abrir</a>"
                    }, {
                        data: null,
                        title: "SMS",
                        defaultContent: "<a id=\"sms-action\" class=\"btn-flat waves-effect center\"> " +
                            "	<i class=\"fas fa-sms\"></i> " +
                            "Abrir</a>"
                    }, {
                        data: null,
                        title: "WhatsApp",
                        defaultContent: "<a id=\"whatsapp-action\" class=\"btn-flat waves-effect center\"> " +
                            "	<i class=\"fab fa-whatsapp\"></i> " +
                            "Abrir</a>"
                    }, {
                        data: "deadline",
                        "title": "Prazo"
                    }, {
                        data: "deadlineType",
                        "title": "Tipo Prazo"
                    }, {
                    	title: "ID",
                        data: "id"
                    }
                ],
                dom: 'Bfrtip',
                autoWidth: false,
                initComplete: function(settings, json) {
                    selectTableConfig(this.DataTable());
                    actionButtons();
                },
                searching: false,
                columnDefs: [{
                    targets: [0, 2],
                    visible: false,
                }, {
                    className: "dt-center",
                    targets: [3, 4, 5, 6, 7, 8, 9]
                }, {
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 1
                }]
            })
        }
    })
}

function save() {
	$("#phase-action").val( phaseId );
	$("#emailText-action").val( getText() );
    if ( $("#action-form").valid() ) {
        $.ajax({
            type: "POST",
            data: $("#action-form").serializeObject(),
            url: "actions",
            success: function (obj) {
                if (obj != false) {
                	table.clear().rows.add(obj).draw();
                    swal("Sucesso!", "O registro foi salvo!", "success");
                    clearActionFields()
                } else {
                    swal("Cancelado", obj.toString(), "error");
                    return 0;
                }
            }
        })
    }
    return false;
}

function actionRemove(id) {
	swal({
		title: "Você tem certeza que deseja cancelar?",
		text: "Não será possível recuperar esse registro após o cancelamento!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Sim!',
		cancelButtonText: "Não!",
		closeOnConfirm: false,
		closeOnCancel: false
	},
	function(isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				data: {
						"id": id
				},
				url: "actionremove",
				success: function(obj) {
					if (obj) {
						 table.row({
			                 selected : true
			               }).remove();
						 table.draw();
						swal("Removido!", "O registro foi removido!", "success");
						clearActionFields()
						selectDeselectTable(table)
					} 
					else {
						swal("Cancelado", "O registro não pode ser removido!", "error");
					} 
				}})
		}
		else
			swal("Cancelado", "O registro não foi removido!", "error");
	});
}

function selectTableConfig(table) {
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	$('.tooltipped').tooltip({delay: 50});
	$('#' +table.tables().nodes().to$().attr('id')+ ' tbody').on('dblclick', 'tr', function () {
		table.rows('.selected').deselect();
		table.rows(this).select()
	} );
}

function selectDeselectTable(table) {
    var selectedRows = table.rows( { selected: true } ).count();
    table.button( 1 ).enable( selectedRows > 0 );
    table.button( 2 ).enable( selectedRows > 0 );
    
    var obj = table.row( { selected: true } ).data();
	var tableId = table.tables().nodes().to$().attr('id')

    if (selectedRows > 0) {
    	switch (tableId) {
    		case 'action-table' : 
    			tableDoubleClick(obj)
    		break;
    		default: return 0;
    	}
    } else {
    	switch (tableId) {
			case 'action-table' : 
				 clearActionFields()
			break;
			default: return 0;
    	}
    }
}

function actionButtons() {
	 $('#action-table tbody').on( 'click', 'a', function (event) {
		 	event.preventDefault()
	        var data = table.row( $(this).parents('tr') ).data();
		 	switch ( $(this).attr("id") ) {
		 		case 'email-action':
		 			openEmailAction(data);
		 			break;
		 		case 'sms-action':
		 			openSmsAction(data);
		 			break;
		 		case 'whatsapp-action':
		 			openWhatsAppAction(data);
		 			break;
		 		default: return 0;
		 	}
	 });
}

function tableDoubleClick(data) {
	refreshInputFields()
	$("#id-action").val(data.id)
	$("#phase-action").val(data.phase != undefined && data.phase != null && data.phase != '' ? data.phase.id : '')
	$("#seqNo-action").val(data.seqNo)
	$("#emailSubject-action").val( data.emailSubject )
	insertHtml(data.emailText);
	$("#smsText-action").val(data.smsText)
	$("#whatsappText-action").val(data.whatsappText)
	$("#deadline-action").val(data.deadline)
}

function openEmailAction(data) {
	console.log(data)
	$('#modal-title').html(data.emailSubject);
	$('#modal-content').html( data.emailText );
	$('#modal').modal('open');
}

function openSmsAction(data) {}

function openWhatsAppAction(data) {}

function validator() {
	$("#action-form").validate({
		errorElement : "div",
		errorPlacement : function(error, element) {
			var er = error.insertAfter(element.next());
			if (er == null)
				er.insertAfter(element.next());
		}
	});
}

function refreshInputFields() {
	$('.input-field label').addClass('active');
	setTimeout(function() {
		$('.input-field label').addClass('active');
	}, 2);
}

function clearActionFields() {
	console.log('hello world')
	$("#id-action").val("");
	$("#action-form")[0].reset();
	removeText()
	table.rows('.selected').deselect();
}

function getParamUrl() {
	var queries = {};
	$.each(document.location.search.substr(1).split('&'), function(c, q) {
		var i = q.split('=');
		try {
			queries[i[0].toString()] = i[1].toString();
		}catch {
			return null;
		}
	});
	return queries;
}

function tiny() {
	!function(e, t, i) {
	    "use strict";
	    tinymce.init({
	        selector: ".tinymce",
	        height: 350,
	        theme: "modern",
	        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools"],
	        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	        toolbar2: "print preview media | forecolor backcolor emoticons",
	        image_advtab: !0,
	        templates: [{
	            title: "Test template 1",
	            content: "Test 1"
	        }, {
	            title: "Test template 2",
	            content: "Test 2"
	        }],
	        content_css: ["//fonts.googleapis.com/css?family=Lato:300,300i,400,400i", "//www.tinymce.com/css/codepen.min.css"]
	    }), tinymce.init({
	        selector: "h2.editable",
	        inline: !0,
	        toolbar: "undo redo",
	        menubar: !1
	    }), tinymce.init({
	        selector: "div.editable",
	        inline: !0,
	        plugins: ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
	        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	    }), tinymce.init({
	        selector: ".tinymce-classic",
	        height: 500,
	        plugins: ["advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker", "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking", "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"],
	        toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
	        toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
	        toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
	        menubar: !1,
	        toolbar_items_size: "small",
	        style_formats: [{
	            title: "Bold text",
	            inline: "b"
	        }, {
	            title: "Red text",
	            inline: "span",
	            styles: {
	                color: "#ff0000"
	            }
	        }, {
	            title: "Red header",
	            block: "h1",
	            styles: {
	                color: "#ff0000"
	            }
	        }, {
	            title: "Example 1",
	            inline: "span",
	            classes: "example1"
	        }, {
	            title: "Example 2",
	            inline: "span",
	            classes: "example2"
	        }, {
	            title: "Table styles"
	        }, {
	            title: "Table row 1",
	            selector: "tr",
	            classes: "tablerow1"
	        }],
	        templates: [{
	            title: "Test template 1",
	            content: "Test 1"
	        }, {
	            title: "Test template 2",
	            content: "Test 2"
	        }],
	        content_css: ["//www.tinymce.com/css/codepen.min.css"]
	    }), tinymce.init({
	        selector: ".tinymce-toolbar",
	        height: 350,
	        toolbar: "mybutton",
	        menubar: !1,
	        setup: function(t) {
	            t.addButton("mybutton", {
	                type: "listbox",
	                text: "My listbox",
	                icon: !1,
	                onselect: function(e) {
	                    t.insertContent(this.value())
	                },
	                values: [{
	                    text: "Menu item 1",
	                    value: "&nbsp;<strong>Some bold text!</strong>"
	                }, {
	                    text: "Menu item 2",
	                    value: "&nbsp;<em>Some italic text!</em>"
	                }, {
	                    text: "Menu item 3",
	                    value: "&nbsp;Some plain text ..."
	                }],
	                onPostRender: function() {
	                    this.value("&nbsp;<em>Some italic text!</em>")
	                }
	            })
	        },
	        content_css: ["//www.tinymce.com/css/codepen.min.css"]
	    })
	}(window, document, jQuery);
}







