const saveUrl = 'config/save';
const listUrl = 'config/list';
const registerUrl = 'config/register';

$(document).ready(function() {
	validate()
});

function save() {
	saveFireSw('<h5>Finalizar Registro</h5>', 'Clique abaixo para continuar.', saveUrl);
	return 0;
}

function saveFireSw(title, text, url) {
    swal({
            title: title,
            text: text,
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Sim!",
            cancelButtonText: "Não",
            html: true
        },
        function(obj) {
            $.ajax({
                type: "POST",
                data:  $(formId).serializeObject(),
                url: contextPath + url,
                success: function (obj) {
                	alerting(obj)
                }
            })
        });
}

function alerting(obj) {
	if (obj.sucesso) {
		alert('success', obj, 'Sucesso', 'Continuar Editando', 'Listar')
	} else {
		alert('error', obj, 'Erro', 'Tentar Novamente', 'Listar')
	}
	return 0;
}

function alert(type, obj, title, confirmButtonText, cancelButtonText) {
	swal({ 
		html:true, 
		type: type,
		title:title, 
		text: '<strong>' + obj.message + '</strong>',
		showCancelButton: true,
		closeOnConfirm: false,
		confirmButtonText: confirmButtonText,
		cancelButtonText: cancelButtonText,
		closeOnCancel: false
	}, 
	function(c) {
		if (c) {
			window.location.href = contextPath + registerUrl + "?id=" + obj.obj.id;
		} else {
			window.location.href = contextPath + listUrl;
		}
	});
}

function validate() {
	$(formId).validate({
		submitHandler : function(form) {
			save();
		},
		rules: {
			smtpPort: {
				digits: true
			}
		},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}
	});
}
