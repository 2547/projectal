var descTempt;
var table;
var table2;
var table3;
var table4;
var phaseId;
var campaignId;
var leadFileButton = $("#lead-file-button");

$(document).ready(function() {
	campaignId = getParamUrl().id;
	validator();
	openTable2(campaignId);
	openTable3(campaignId);
	clearFormFields();
	testEmailConfigClick();
	mask();
	uploadLeads();
	onClickFuntions()
});

function onClickFuntions() {
	$("#qty-logins").click(function () {
		qtyLoginsSwalInput()
	})
}

function qtyLoginsSwalInput() {
    swal({
            title: "Solicitar quantidade de logins",
            text: "Entre com a quantidade de logins que deseja solicitar",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            confirmButtonText: "Sim!",
            cancelButtonText: "Não",
            inputPlaceholder: "Quantidade"
        },
        function(inputValue) {
            if (inputValue === false) return false;
            if ( inputValue === "" || !inputValue.match(/^-{0,1}\d+$/) ) {
                swal.showInputError("Número inválido!");
                return false
            }
//            swal("Nice!", "You wrote: " + inputValue, "success");
        });
}

function uploadLeads() {
    $('#lead-file').change(function() {
        try {
            formdata = new FormData();
            if ($(this).prop('files').length > 0) {
                file = $(this).prop('files')[0];
                formdata.append("file", file);
                formdata.append("phaseId",  $("#id-phase").val());
            }
            swal({
                    title: "Preparando para enviar o arquivo (" + file.name + ")",
                    text: "<strong> Tem certeza que deseja continuar? Isso pode demorar alguns minutos. </strong>",
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "Sim!",
                    cancelButtonText: "Não",
                    html:true
                },
                function(obj) {
                    $.ajax({
                        url: "uploadleads",
                        type: "POST",
                        data: formdata, 
                        processData: false,
                        contentType: false,
                        success: function(obj) {
                        	$('#lead-file').val("")
                        	fileUploadSuccessAlert(obj)
                        }
                    });
                });
            
        } catch (error) {
            console.log(error)
        }
    });
}

function fileUploadSuccessAlert(obj) {
	if (obj.success) {
		swal({ 
			 html:true, 
			 type: 'success',
			 title:'Sucesso', 
			 text: '<strong>' + obj.message + '</strong>'
		});
	} else {
		swal({ 
			 html:true, 
			 type: 'error',
			 title:'Erro', 
			 text: '<strong>' + obj.message + '</strong>'
		});
	}
	return 0;
}

function testEmailConfigClick() {
	$("#test-email-config").click(function (){
		testEmailConfig();
	})
}

function actionModal() {
	$('#action-modal').modal();
	$('#action-modal').modal('open');
}

function mask()
{
	$('.phone').mask('(00) 00000-0000');
}

function openTable2(id) {
	campaignId = id;
	if(id == null || undefined || '')
		return 0;
	
	if (table2 != undefined && table2 != '')
		table2.destroy();
	
	$.ajax({
		url : "getphases",
		data : {"campaignId" : id },
		success : function(obj) 
		{
			table2 = $('#phase-table').DataTable( {
				data : obj,
				"language" : {
					"url" : "/alc/resources/vendors/data-tables/json/Portuguese-Brasil.json"
				},
				"searching": false,
				responsive: true,
				columns : 	[	
								{ data: "id"}, {
									data : null, "defaultContent": '', 'title': ''
								},
								{ data: "name", "title": "Nome"},
								{ data: "description", "title": "Descrição"},
								{ data: "seqNo", "title": "Nro Seq", "width": "10px"},
								{ data: "url", "title": "URL"},
								{ data: "campaign",
				    				"mRender": function(data, type, full)
				    				{
				    					return full.campaign.id;
				    				}
								}
							],
				dom : 'Bfrtip',
				select : 'single',
				autoWidth: false,
				buttons: [ {
			                text: '<i class="material-icons left">clear_all</i></a>',
			                className : 'btn-floating waves-effect waves-light purple lightrn-1 tooltipped',
			                attr:  {
			                	 'data-position':'top', 
			                	 'data-delay':'50',
			                	 'data-tooltip': 'Limpar Formulário'
			                },
			                action: function ( e, dt, node, config ) {
			                	 clearPhaseFields()
			                },
			                enabled: true
			            }, {
			                text: '<i class="material-icons left ">clear</i></a>',
			                className : 'btn-floating waves-effect waves-light red tooltipped',
			                attr: {
			                	 'data-position':'top', 
			                	 'data-delay':'50',
			                	 'data-tooltip': 'Remover Registro'
			                },
			                action: function ( e, dt, node, config ) {
			                	var obj = dt.row( { selected: true } ).data();
			                	phaseId = obj.id;
			                	phaseRemove(phaseId);
			                },
			                enabled: false
			            }, {
			                text: '<i class="material-icons left">add</i>Adicionar Ação</a>',
			                className : 'waves-effect waves-light btn box-shadow-none border-round mr-1 tooltipped',
			                attr:  {
			                	 'data-position':'top', 
			                	 'data-delay':'50',
			                	 'data-tooltip': 'Adicionar Ação a Fase Selecionada Abaixo'
			                },
			                action: function ( e, dt, node, config ) {
			                	var obj = dt.row( { selected: true } ).data();
			                	phaseId = obj.id;
			                	window.location.href = contextPath + "action/register?phaseId="+phaseId+"&campaignId="+campaignId;
			                },
			                enabled: false
			            }, {
			                text: '<i class="material-icons right">send</i>',
			                className : 'btn waves-effect waves-light cyan right tooltipped',
			                attr:  {
			                	 'data-position':'top', 
			                	 'data-delay':'50',
			                	 'data-tooltip': 'Enviar Alteração'
			                },
			                action: function ( e, dt, node, config ) {
			                	save2()
			                },
			                enabled: true
			            },
			    ],
			    initComplete: function(settings, json) {
			    	selectTableConfig(this.DataTable());
			    },
				select : true,
				columnDefs : [ {
					targets : [ 0, 6 ],
					visible : false,
				}, { 	
					className: "dt-center", 
					targets: [2, 3, 4] 
				}, {
					orderable : false,
					className : 'select-checkbox',
					targets : 1
				}],
				select : {
					style : 'os',
					selector : 'td:first-child'
				}
			})
		}
	})
}

function openTable3(id) {
	
	if(id == null || undefined || '')
		return 0;
	
	if (table3 != undefined && table3 != '')
		table3.destroy();
	
	$.ajax({
		url : "getlogins",
		data : { 
			"campaignId" : id
		},
		success : function(obj) {
			console.log(obj)
			table3 = $('#login-table').DataTable({
				"sPaginationType" : "full_numbers",
				data : obj,
				"language" : {
					"url" : "/alc/resources/vendors/data-tables/json/Portuguese-Brasil.json"
				},
				columns : [	
							{ data: "id"}, 
							{ data: "name", "title": "Nome"},
							{ data: "login", "title": "Login", "width": "400px"},
							{ data: "email", "title": "Email", "width": "400px"},
							{data: "cellphone", "title": "Telefone", "width": "400px"},
							{ data: "campaign",
			    				"mRender": function(data, type, full) {
			    					return full.campaign.id;
			    				}
							}
				],
				buttons: [ {
	                text: '<i class="material-icons left">clear_all</i></a>',
	                className : 'btn-floating waves-effect waves-light purple lightrn-1 tooltipped',
	                attr:  {
	                	 'data-position':'top', 
	                	 'data-delay':'50',
	                	 'data-tooltip': 'Limpar Formulário'
	                },
	                action: function ( e, dt, node, config ) {
	                	 clearLoginFields()
	                },
	                enabled: true
	            }, {
	                text: '<i class="material-icons left ">clear</i></a>',
	                className : 'btn-floating waves-effect waves-light red tooltipped',
	                attr:  {
	                	 'data-position':'top', 
	                	 'data-delay':'50',
	                	 'data-tooltip': 'Remover Registro'
	                },
	                action: function ( e, dt, node, config ) {
	                	var obj = dt.row( { selected: true } ).data();
	                	loginRemove(obj.id)
	                },
	                enabled: false
	            }, {
	                text: '<i class="material-icons right">send</i>',
	                className : 'btn waves-effect waves-light cyan right',
	                action: function ( e, dt, node, config ) {
	                	var obj = dt.row( { selected: true } ).data();
	                	save3()
	                },
	                enabled: true
	            },
	           ],
				dom : 'Bfrtip',
				autoWidth: false,
				select : 'single',
				searching: false,
		        select : {
					style : 'os',
					selector : 'td:first-child'
				},
				initComplete: function(settings, json) {
					selectTableConfig(this.DataTable());
			    },
				columnDefs : [{
					targets : [ 0, 5 ],
					visible : false,
				}, { 
					className: "dt-left", 
					targets: [] 
				}]
			})
		}
	})
}

function selectTableConfig(table) {
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	$('.tooltipped').tooltip({delay: 50});
	
	$('#' +table.tables().nodes().to$().attr('id')+ ' tbody').on('dblclick', 'tr', function () {
		table.rows('.selected').deselect();
		table.rows(this).select()
	} );
}

function selectDeselectTable(table) {
    var selectedRows = table.rows( { selected: true } ).count();
    
    table.button( 1 ).enable( selectedRows > 0 && selectedRows < 2 );
    table.button( 2 ).enable( selectedRows > 0 && selectedRows < 2 );
    
    var obj = table.row( { selected: true } ).data();
	var tableId = table.tables().nodes().to$().attr('id')
	
    if (selectedRows > 0) {
    	switch (tableId) {
    		case 'phase-table' : 
    			goToByScroll("phone")
    			leadFileButton.removeClass('disabled')
    			table2Click(obj)
    		break;
    		case 'login-table' : 
    			table3Click(obj)
    		break;
    		default: return 0;
    	}
    } else {
    	switch (tableId) {
			case 'phase-table' :
				 leadFileButton.addClass('disabled')
				 clearPhaseFields()
			break;
			case 'login-table' : 
    			table3Click(obj)
    		break;
			default: return 0;
    	}
    }
}

function table2Click(data) {
	refreshInputFields()
	$("#id-phase").val(data.id)
	$("#campaign-phase").val(data.campaign.id)
	$("#name-phase").val(data.name)
	$("#description-phase").val(data.description)
	$("#seqNo-phase").val(data.seqNo)
	$("#url-phase").val(data.url)
}

function table3Click(data) {
	refreshInputFields()
	$("#id-login").val(data.id)
	$("#campaign-login").val(data.campaign.id)
	$("#name-login").val(data.name)
	$("#login").val(data.login)
	$("#email-login").val(data.email)
	$("#cellphone").val(data.cellphone)
}

function table4Click(data) {
	refreshInputFields()
	$("#id-action").val(data.id)
	$("#phase-action").val(data.phase.id)
	$("#seqNo-action").val(data.seqNo)
	$("#emailSubject-action").val(data.emailSubject)
	$("#emailText-action").val(data.emailText)
	$("#smsText-action").val(data.smsText)
	$("#whatsappText-action").val(data.whatsappText)
	$("#deadline-action").val(data.deadline)
}

function testEmailConfig()
{
	 swal({   
			  title: "Enviar email para teste",
		      text: "Tem certeza que deseja continuar?",
		      type: "info",   
		      showCancelButton: true,
		      closeOnConfirm: false,
		      showLoaderOnConfirm: true,
		      confirmButtonText: "Sim!",
	          cancelButtonText: "Não",
	      },
	      function() {
	    	  
			  $.ajax({
	    	        type: "GET",
	    	        data: {
	    	        	"host": $("#smtpHost").val(),
	    	        	"port": $("#smtpPort").val(),
	    	        	"username": $("#smtpUser").val(),
	    	        	"password": $("#smtpPassword").val(),
	    	        	"to": $("#smtpSender").val(),
	    	        	"from": $("#smtpSender").val()
	    	        },
	    	        url: "testemailconfig",
	    	        success: function (obj) {
	    	        	 swal(obj);  
	    	        }
	    	    }) 
	  });
}

function testEmailConfigValidator() {
	var isValid = true;
	
	if ($("#smtpHost").val() == null || $("#smtpHost").val() == undefined || $("#smtpHost").val() == '') {
		isValid = false;
	}
	if ($("#smtpPort").val() == null || $("#smtpPort").val() == undefined || $("#smtpPort").val() == '') {
		isValid = false;
	}
	if ($("#smtpUser").val() == null || $("#smtpUser").val() == undefined || $("#smtpUser").val() == '') {
		isValid = false;
	}
	if ($("#smtpPassword").val() == null || $("#smtpPassword").val() == undefined || $("#smtpPassword").val() == '') {
		isValid = false;
	}
	if ($("#smtpSender").val() == null || $("#smtpSender").val() == undefined || $("#smtpSender").val() == '') {
		isValid = false;
	}
	if ($("#smtpSender").val() == null || $("#smtpSender").val() == undefined || $("#smtpSender").val() == '') {
		isValid = false;
	}
	
	return isValid;
}

function save() {
    if ($("#campaign-form").valid()) {
        $.ajax({
            type: "POST",
            data: $("#campaign-form").serializeObject(),
            url: "campaign",
            success: function (obj) {
            	var campaign = obj.campaign;
            	var isEditing = obj.isEditing;
            	
            	if (campaign != null && campaign != '' && campaign != undefined) {
            		campaignId = campaign.id;
            		
	             	if (isEditing) {
	             		swal("Sucesso!", "O registro foi atualizado!", "success");
	             	}
	             	else {
	             		swal({  title: "Registro Salvo com Sucesso",
		                        text: "Deseja cadastrar uma fase a essa campanha?",
		                        type: "success",
		                        showCancelButton: true,
		                        confirmButtonColor: "#2979ff",
		                        confirmButtonText: "Sim, Cadastrar Fase",
		                        cancelButtonText: "Não",
		                        closeOnConfirm: true,
		                        closeOnCancel: true 
	                        },
	                        function(isConfirm){
	                        if (isConfirm) {
	                        	$("#phase-div").removeClass('hide');
	                        	goToByScroll("phone");
	                        	openTable2(campaignId);
	                        }
	                        else {
	                        	window.location.href = "campaignlist";
	                        }
	                      });
	             		
	             	}
	            } else {
	                swal("Cancelado", obj.toString(), "error");
	                return 0;
	            }
            }
        })
    }
    return false;
}

function save2() {
	$("#campaign-phase").val(campaignId);
	
    if ($("#phase-form").valid()) {
        $.ajax({
            type: "POST",
            data: $("#phase-form").serializeObject(),
            url: "phase",
            success: function (obj) {
            	console.log('hi')
            	clearPhaseFields()
                if (obj != false) {
                	table2.clear().rows.add(obj).draw();
                        swal("Sucesso!", "O registro foi salvo!",
                            "success");
                } else {
                    swal("Cancelado",
                    	obj.toString(),
                        "error");
                    return 0;
                }
            }
        })
    }
    return false;
}

function save3() {
	$("#campaign-login").val(getParamUrl().id);
    if ($("#login-form").valid()) {
        $.ajax({
            type: "POST",
            data: $("#login-form").serializeObject(),
            url: "logins",
            success: function (obj) {
            	
                if (obj != false) {
                	table3.clear().rows.add(obj).draw();
                        swal("Sucesso!", "O registro foi salvo!",
                            "success");
                        $("#id-login").val("");
                        $("#login-form")[0].reset()
                        $("#login-form-delete").addClass("hide")
                } else {
                    swal("Cancelado",
                    	obj.toString(),
                        "error");
                    return 0;
                }
            }
        })
    }
    return false;
}

function phaseRemove(id) {
	swal({
		title: "Você tem certeza que deseja cancelar?",
		text: "Não será possível recuperar esse registro após o cancelamento!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Sim!',
		cancelButtonText: "Não!",
		closeOnConfirm: false,
		closeOnCancel: false
	},
	function(isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				data:  {"id": id },
				url: "phaseremove",
				success: function(obj) {
					if (obj) {
						 table2.row({
			                 selected : true
			               }).remove();
						 table2.draw();
						 selectDeselectTable(table2)
						swal("Removido!", "O registro foi removido!", "success");
						clearPhaseFields()
					} 
					else {
						swal("Cancelado", "O registro não pode ser removido!", "error");
					} 
				}})
		}
		else
			swal("Cancelado", "O registro não foi removido!", "error");
	});
}

function loginRemove(id)
{
	swal({
		title: "Você tem certeza que deseja cancelar?",
		text: "Não será possível recuperar esse registro após o cancelamento!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Sim!',
		cancelButtonText: "Não!",
		closeOnConfirm: false,
		closeOnCancel: false
	},
	function(isConfirm)
	{
		if (isConfirm)
		{
			$.ajax({
				type: "POST",
				data:  {"id": $("#id-login").val()},
				url: "loginremove",
				success: function(obj)
				{
					if (obj)
					{
						 table3.row({
			                 selected : true
			               }).remove();
						 table3.draw();
						 swal("Removido!", "O registro foi removido!", "success");
						 clearLoginFields()
					} 
					else 
					{
						swal("Cancelado", "O registro não pode ser removido!", "error");
					} 
				}})
		}
		else
			swal("Cancelado", "O registro não foi removido!", "error");
	});
}

function validator() {
	jQuery.validator.addMethod(
			"validDate",
			function(value, element) {
			    return value.match(/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/);
			},
			"Please enter a valid date in the format DD/MM/YYYY"
	);

	$("#campaign-form").validate({
		submitHandler : function(form) {
			save();
		},
		rules: {
			registerUrl: {
		      url: true
		    }
		},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				error.insertAfter(element);
			}
		}
	});

	$("#phase-form").validate({
		 submitHandler: function(form) {
		    save2();
		},
		 rules: {
		    url: {
		      url: true
		    },
			seqNo : {
				required : true,
				remote : {
					url : "isuniqueseqno",
					type : "POST",
					data : {
						campaignId : function() {
							return campaignId;
						},
						seqNo: function() {
							return $("#seqNo-phase").val()
						},
						phaseId: function() {
							return $("#id-phase").val()
						}
					},
					dataFilter : function(response)
					{
						var response = jQuery.parseJSON(response);
						currentMessage = response.Message;
						
						if (response) {
							return false;
						}
						return true;
					}
				}
			}
		},
		errorElement : "div",
		errorPlacement : function(error, element) {
			var er = error.insertAfter(element.next());

			if (er == null)
				er.insertAfter(element.next());

		}
	});
	 
	$("#login-form").validate({
		submitHandler: function(form) {
		    save3();
		},
		errorElement : "div",
		errorPlacement : function(error, element) {
			var er = error.insertAfter(element.next());

			if (er == null)
				er.insertAfter(element.next());

		}
	});
}

function getParamUrl() {
	var queries = {};
	$.each(document.location.search.substr(1).split('&'), function(c, q) {
		var i = q.split('=');
		try {
			queries[i[0].toString()] = i[1].toString();
		}catch {
			return null;
		}
	});
	return queries;
}

function refreshInputFields() {
	$('.input-field label').addClass('active');
	setTimeout(function() {
		$('.input-field label').addClass('active');
	}, 2);
}

function clearFormFields() {
	
	$("#login-form-clear").click(function (){
		$("#id-login").val("");
		$("#login-form")[0].reset();
		$("#login-form-delete").addClass("hide")
	})
}

function clearPhaseFields() {
	$("#id-phase").val("");
	$("#phase-form")[0].reset();
	table2.rows('.selected').deselect();
}

function clearLoginFields() {
	$("#id-login").val("");
	$("#login-form")[0].reset();
	table3.rows('.selected').deselect();
}

function goToByScroll(id) {
    id = id.replace("link", "");
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    }, 'slow');
}

