var table;
var registerPath = 'config/register';
var removePath = 'config/remove';

$(document).ready(function() {
	table()
})

function table() {
    table = $(tableId)
        .DataTable({
        	ajax: {
				type: "GET",
				url: contextPath + "config/getlist",
				dataSrc:""
			},
            language: getLanguage(),
            columns: getColumns(),
            buttons: getButtons(),
            columnDefs: getColumnDefs(),
            initComplete: function(settings, json) {
                selectTableConfig(this.DataTable());
            },
            dom: 'Bfrtip',
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function addButtonBlock() {
	var rows = table.data().count();
	table.button(0).enable( rows == 0  );
}

function selectTableConfig(table) {
	addButtonBlock()
	let tableId = '#' +table.tables().nodes().to$().attr('id');
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	
	$(tableId + ' tbody').on('dblclick', 'tr', () => {
		table.rows(this).select()
		var obj = table.row( { selected: true } ).data();
		window.location.href = contextPath + registerPath + "?id=" + obj.id;
	} );
}

function getTableId (table) {
	return '#'+table.tables().nodes().to$().attr('id');
}

function selectDeselectTable(table) {
	var selectedRows = table.rows( { selected: true } ).count();
    table.buttons( [1, 2 ]).enable( selectedRows > 0  );
    addButtonBlock()
}

function promtpRemove(id) {
	swal({ 
		html:true, 
		type: 'warning',
		title: 'Remover', 
		text: '<strong>Tem certeza que deseja remover esse registro</strong>',
		showCancelButton: true,
		closeOnConfirm: false,
		confirmButtonText: 'Sim',
		cancelButtonText: 'Não',
		closeOnCancel: false
	}, 
	function(c) {
		if (c) {
			remove(id)
		} else {
			
		}
	});
}

function remove(id) {
	$.ajax({
		type: "POST",
		data: {
			id : id
		},
		url : contextPath + removePath,
		success : function(obj) {
			if (obj === true) {
				 table.row({
	                 selected : true
	               }).remove();
				 table.draw();
				 
				 addButtonBlock()
				swal("Removido!", "Registro removido com sucesso.", "success");
			} else {
				console.log(obj);
				swal("Erro ao Remover!", "Desculpe, mas não foi possível remover esse registro!", "error");
			}
		}
	})
}

function getButtons() {
    return [{
            text: '<i class="material-icons left">add</i></a>',
            className: 'btn-floating waves-effect waves-light purple lightrn-1',
            action: function(e, dt, node, config) {
                window.location.href = contextPath + "config/register";
            },
            enabled: false
        }, {
            text: '<i class="material-icons left">edit</i></a>',
            className: 'btn-floating waves-effect',
            action: function(e, dt, node, config) {
                var obj = dt.row({
                    selected: true
                }).data();
                window.location.href = contextPath + registerPath + "?id=" + obj.id;
            },
            enabled: false
        }, {
            text: '<i class="material-icons left ">clear</i></a>',
            className: 'btn-floating waves-effect waves-light red',
            action: function(e, dt, node, config) {
                var obj = dt.row({
                    selected: true
                }).data();
                promtpRemove(obj.id);
            },
            enabled: false
        }
    ];
}

function formatDate(date) {
	var dd = date.getDate();
	var mm = date.getMonth() + 1; //January is 0!
	var yyyy = date.getFullYear();
	if (dd < 10) {
	  dd = '0' + dd;
	} 
	if (mm < 10) {
	  mm = '0' + mm;
	} 
	return today = dd + '/' + mm + '/' + yyyy;
}

function getColumns() {
    return [{
            title: "",
            data: "id"
        }, {
            title: "created",
            data: "created"
        }, {
            title: "Host",
            data: "smtpHost"
        }, {
            title: "Porta",
            data: "smtpPort"
        }, {
            title: "SMTP Senha",
            data: "smtpPassword",
        }, {
            title: "SMTP Remetente",
            data: "smtpSender",
        }
    ];
}

function getColumnDefs() {
	return [{
        targets: [0, 1],
        visible: false
    },{
        className: "text-capitalize text-truncate text-center",
        targets: "_all"
    }];
}

